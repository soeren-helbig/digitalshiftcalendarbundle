<?php

namespace Digitalshift\CalendarBundle\AppointmentWrappers;

use DateTime;
use Digitalshift\CalendarBundle\AppointmentWrappers\Day;
use Digitalshift\CalendarBundle\AppointmentWrappers\Month;
use Digitalshift\CalendarBundle\AppointmentWrappers\Week;
use Digitalshift\CalendarBundle\Holidays\HolidayReceiverInterface;
use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use Doctrine\ORM\EntityManager;

/**
 * WrapperService to manage AppointmentWrappers like day, week and month.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class WrapperService
{
    const REPOSITORY = 'DigitalshiftCalendarBundle:Appointment';

    private $entityManager, $dateTimeHelper, $appointmentExtensions, $holidayReceiver;

    /**
     * @param EntityManager $entityManager
     * @param DateTimeHelper $dateTimeHelper
     */
    public function __construct(
        EntityManager $entityManager,
        DateTimeHelper $dateTimeHelper,
        $appointmentExtensions = array(),
        HolidayReceiverInterface $holidayReceiver
    ) {
        $this->entityManager  = $entityManager;
        $this->dateTimeHelper = $dateTimeHelper;
        $this->appointmentExtensions = $appointmentExtensions;
        $this->holidayReceiver = $holidayReceiver;
    }

    /**
     * get day-wrapper with according appointments.
     *
     * @param DateTime $date
     */
    public function getDay(DateTime $date)
    {
        $repository   = $this->entityManager->getRepository(static::REPOSITORY);
        $appointments = $repository->findByDay($date);
        $holidays     = $this->holidayReceiver->fetchByDate($date);

        return new Day($start, $appointments, null, $holidays);
    }

    /**
     * get week-wrapper with according appointments. if week of year is set,
     * the week number accorderung to startDate is returned.
     *
     * @param DateTime $start
     * @param boolean $weekOfYear
     */
    public function getWeek(DateTime $start, $weekOfYear = true)
    {

        $start = ($weekOfYear) ? $this->dateTimeHelper->getWeekStart($start) : $start;
        $end   = $this->dateTimeHelper->getWeekEnd($start);

        $repository   = $this->entityManager->getRepository(static::REPOSITORY);
        $appointments = $repository->findByPeriod($start, $end);
        $holidays     = $this->holidayReceiver->fetchByDate($start, $end);

        return new Week($start, $appointments, $weekOfYear, null, $holidays);
    }

    /**
     * get month-wrapper with according appointments. if month of year is set,
     * the month number accorderung to startDate is returned.
     *
     * @param DateTime $start
     * @param boolean $monthOfYear
     */
    public function getMonth(DateTime $date, $monthOfYear = true)
    {
        $firstDayOfMonth = $date->format('t');
        $lastDayOfMonth  = $date->format('t');
        $start = new DateTime($date->format('Y').'-'.$date->format('m').'-1');
        $end   = new DateTime($date->format('Y').'-'.$date->format('m').'-'.$date->format('t'));

        if ($monthOfYear)
        {
            $weekRange = $this->dateTimeHelper->getWeekRange($start);
            $start     = $weekRange->getStart();

            $weekRange = $this->dateTimeHelper->getWeekRange($end);
            $end       = $weekRange->getEnd();
        }

        $repository   = $this->entityManager->getRepository(static::REPOSITORY);
        $appointments = $repository->findByPeriod($start, $end);
        $holidays     = $this->holidayReceiver->fetchByDate($start, $end);

        return new Month($start, $appointments, $monthOfYear, null, $holidays);
    }

    /**
     * @return DateTimeHelper
     */
    public function getDateTimeHelper()
    {
        return $this->dateTimeHelper;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }
}