<?php

namespace Digitalshift\CalendarBundle\AppointmentWrappers;

use DateTime;
use Digitalshift\CalendarBundle\AppointmentWrappers\Week;
use Digitalshift\CalendarBundle\Entity\HolidayCollection;
use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use Digitalshift\CalendarBundle\Library\DateTimeRange;

/**
 * MonthWrapper to group appointments in months.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class Month extends DayCollection
{
    private $weeks;

    /**
     * @param DateTime $start
     * @param array $appointments
     * @param boolean $monthOfYear
     * @param DateTimeHelper $dateTimeHelper
     *
     * @throws InvalidArgumentException
     */
    public function __construct(
        DateTime $start,
        $appointments = array(),
        $monthOfYear = true,
        DateTimeHelper $dateTimeHelper = null,
        HolidayCollection $holidays = null
    ) {
        parent::__construct(
            $start,
            DateTimeHelper::TIME_MONTH,
            $appointments,
            $dateTimeHelper,
            $holidays
        );

        $this->weeks = array();
    }

    /**
     * @return array
     */
    public function getWeeks()
    {
        $startDate = parent::getStartDate();
        $endDate  = parent::getEndDate();

        $weekRanges  = $this->dateTimeHelper->getWeekRangesForMonth(parent::getStartDate(), parent::getEndDate());
        $this->weeks = array();

        /** @var DateTimeRange $weekRange */
        foreach ($weekRanges as $weekRange) {
            $appointments = parent::getAppointmentsForTimeRange($weekRange->getStart(), DateTimeHelper::TIME_WEEK, false);
            $this->weeks[] = new Week($weekRange->getStart(), $appointments);
        }

        return $this->weeks;
    }
}