<?php

namespace Digitalshift\CalendarBundle\AppointmentWrappers;

use DateTime;
use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Digitalshift\CalendarBundle\Entity\HolidayCollection;
use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use InvalidArgumentException;

/**
 * WeekWrapper to group appointments in months.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class Week extends DayCollection
{
    /**
     * @param DateTime $start
     * @param array $appointments
     * @param boolean $weekOfYear
     * @param DateTimeHelper $dateTimeHelper
     *
     * @throws InvalidArgumentException
     */
    public function __construct(
        DateTime $start,
        $appointments = array(),
        $weekOfYear = true,
        DateTimeHelper $dateTimeHelper = null,
        HolidayCollection $holidays = null
    ) {
        parent::__construct(
            $start,
            DateTimeHelper::TIME_WEEK,
            $appointments,
            $dateTimeHelper,
            $holidays
        );
    }

    /**
     * @return integer
     */
    public function getWeekNumber()
    {
        return $this->dateTimeHelper->getWeekNumber(parent::getStartDate());
    }
}