<?php

namespace Digitalshift\CalendarBundle\AppointmentWrappers;

use DateTime;
use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Digitalshift\CalendarBundle\Appointments\FilterInterface;
use Digitalshift\CalendarBundle\Appointments\AppointmentCollection;
use Digitalshift\CalendarBundle\Entity\HolidayCollection;
use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use InvalidArgumentException;

/**
 * Day-Wrapper to group appointments by day.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class Day
{
    private $date, $holidays, $appointments, $dateTimeHelper;

    /**
     * @param DateTime $date      - date of day
     * @param array $appointments - array of AppointmentInterface - instances
     */
    public function __construct(DateTime $date, AppointmentCollection $appointments = null, DateTimeHelper $dateTimeHelper = null, HolidayCollection $holidays = null)
    {
        $this->date           = $date;
        $this->holidays       = ($holidays)       ? $holidays       : new HolidayCollection();
        $this->appointments   = ($appointments)   ? $appointments   : new AppointmentCollection();
        $this->dateTimeHelper = ($dateTimeHelper) ? $dateTimeHelper : new DateTimeHelper();
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param AppointmentInterface $appointment
     *
     * @throws InvalidArgumentException
     */
    public function addAppointment(AppointmentInterface $appointment)
    {

        if (!$appointment->isValidInPeriod($this->getStart(), $this->getEnd()))
        {
            throw new InvalidArgumentException();
        }

        $this->appointments->add($appointment);
    }

    /**
     * @param FilterInterface $appointmentFilter
     */
    public function getAppointments(FilterInterface $appointmentFilter = null)
    {
//         return $appointmentFilter->match($this->appointments);
        return $this->appointments;
    }

    /**
     * @return DateTime
     */
    public function getStart()
    {
        return $this->dateTimeHelper->getDayStart($this->date);
    }

    /**
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->dateTimeHelper->getDayEnd($this->date);
    }

    /**
     * @param integer $start
     * @param integer $duration
     * @param boolean $beforeStart
     * @param boolean $afterEnd
     */
    public function getAppointmentsForTimeRange($start, $duration, $beforeStart = true, $afterEnd = true)
    {
        $appointmentsInRange = array();

        $startDate = clone $this->date;
        $startDate->setTimestamp($startDate->getTimestamp() + $start);

        $endDate = new DateTime();
        $endDate -> setTimestamp($startDate->getTimestamp() + $duration);


        foreach ($this->appointments as $appointment)
        {
            if ($appointment->isValidInPeriod($startDate, $endDate, $beforeStart, $afterEnd))
            {
                $appointmentsInRange[] = $appointment;
            }
        }

        return $appointmentsInRange;
    }

    /**
     * @return HolidayCollection
     */
    public function getHolidays()
    {
        return $this->holidays;
    }
}