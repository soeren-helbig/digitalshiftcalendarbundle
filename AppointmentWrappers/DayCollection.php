<?php

namespace Digitalshift\CalendarBundle\AppointmentWrappers;

use DateTime;
use Digitalshift\CalendarBundle\Appointments\AppointmentCollection;
use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Digitalshift\CalendarBundle\Entity\HolidayCollection;
use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use InvalidArgumentException;
use IteratorAggregate;
use ArrayIterator;

/**
 * MultiDayInterface to group appointments in different ways, e.g. weeks, months.
 * Its time-period is set by the array of days.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class DayCollection implements IteratorAggregate
{
    const PERIOD_START = 0;
    const PERIOD_END   = 1;

    protected
        $dateTimeHelper,
        $days;

    public function __construct(
        DateTime $startDate = null,
        $duration = null,
        $appointments = array(),
        DateTimeHelper $dateTimeHelper = null,
        HolidayCollection $holidays = null
    ) {

        if ($duration < 0) {
            throw new InvalidArgumentException();
        }

        $this->dateTimeHelper = ($dateTimeHelper) ? $dateTimeHelper : new DateTimeHelper();

        $this->buildDays($startDate, $duration, $holidays);

        /** @var AppointmentInterface $appointment */
        foreach ($appointments as $appointment) {
            $this->addAppointment($appointment);
        }
    }

    /**
     * @param DateTime $date
     * @param integer $duration
     * @param array $appointments
     *
     * @throws InvalidArgumentException
     */
    protected function buildPeriod(
        DateTime $startDate = null,
        $duration = null,
        $appointments = array(),
        HolidayCollection $holidays = null
    ) {
        if ($duration < 0) {
            throw new InvalidArgumentException();
        }

        $this->buildDays($startDate, $duration, $holidays);

        /** @var AppointmentInterface $appointment */
        foreach ($appointments as $appointment) {
            $this->addAppointment($appointment);
        }
    }

    /**
     * @param DateTime $startDate
     * @param integer $duration
     */
    private function buildDays(DateTime $startDate, $duration, HolidayCollection $holidays = null)
    {
        $this->days = array();
        $start      = new DateTime($startDate->format('Y-m-d'));
        $startTime  = $start->getTimestamp();
        $end        = $startTime + $duration;

        for ($date = $startTime; $date < $end; $date += DateTimeHelper::TIME_DAY) {
            $tmpDate = new DateTime();
            $tmpDate -> setTimestamp($date);

            $day = new Day(
                $tmpDate,
                null,
                null,
                $this->hydrateHolidaysForDay($tmpDate, $holidays)
            );

            $this->days[$this->dateTimeHelper->getDayStart($tmpDate)->getTimestamp()] = $day;
        }
    }

    /**
     * @param \DateTime $date
     * @param HolidayCollection $holidays
     * @return HolidayCollection
     */
    private function hydrateHolidaysForDay(\DateTime $date, HolidayCollection $holidays = null)
    {
        return ($holidays) ? $holidays->getByDate($date) : null;
    }

    /**
     * private helper to check whether an appoint is part of the given
     * time-period.
     *
     * @param DateTime $date
     * @param integer $duration
     * @param AppointmentInterface $appointment
     */
    private function isAppointmentInPeriod(DateTime $date, $duration, AppointmentInterface $appointment)
    {
        $start = $date->getTimestamp();

        $end = new DateTime();
        $end -> setTimestamp($start + $duration);

        $appointmentTime = $appointment->getDate()->getTimestamp();

        return ($appointmentTime > $start && $appointmentTime < $end) ? true : false;
    }

    /**
     * @param AppointmentInterface $appointment
     * @param boolean $force - force adding (needed if new appointment is out of period)
     *
     * @throws InvalidArgumentException
     */
    public function addAppointment(AppointmentInterface $appointment, $force = false)
    {
        $start = $this->getStartDate();
        $end = $this->getEndDate();

        // argument check
        if (!$appointment->isValidInPeriod($this->getStartDate(), $this->getEndDate()))
        {
            throw new InvalidArgumentException();
        }

        $dayIndex = $this->dateTimeHelper->getDayStart($appointment->getStart())->getTimestamp();
        $day      = (!isset($this->days[$dayIndex])) ? reset($this->days) : $this->days[$dayIndex];
        $day      -> addAppointment($appointment);
    }

    /**
     * @return DateTime
     */
    public function getStartDate()
    {
        $period = $this->days;
        reset($period);
        $periodStart = key($period);

        $startDate = new DateTime();
        $startDate -> setTimestamp($periodStart);

        return $startDate;
    }

    /**
     * @return DateTime
     */
    public function getEndDate()
    {
        $period        = $this->days;
        $periodEndDay  = end($period);
        $periodEnd     = mktime(23, 59, 59, $periodEndDay->getDate()->format('n'), $periodEndDay->getDate()->format('j'), $periodEndDay->getDate()->format('Y'));

        $endDate = new DateTime();
        $endDate -> setTimestamp($periodEnd);

        return $endDate;
    }

    /**
     * (non-PHPdoc)
     * @see IteratorAggregate::getIterator()
     */
    public function getIterator()
    {
        return new ArrayIterator($this->days);
    }

    /**
     * @param integer $start
     * @param integer $duration
     * @param boolean $beforeStart
     * @param boolean $afterEnd
     */
    public function getAppointmentsForDailyTimeRange($start, $duration, $beforeStart = true, $afterEnd = true)
    {
        $appointments = array();

        foreach ($this->days as $timestamp => $day)
        {
            $appointments[] = $day->getAppointmentsForTimeRange($start, $duration, $beforeStart, $afterEnd);
        }

        return $appointments;
    }

    /**
     * @todo retrieve appointments for range
     *
     * @param DateTime $start
     * @param integer $duration
     * @param boolean $beforeStart
     * @param boolean $afterEnd
     */
    public function getAppointmentsForTimeRange(DateTime $start, $duration, $beforeStart = true, $afterEnd = true)
    {
        $days = $this->getDaysInTimeRange($start, $duration);
        $appointments = array();

        foreach ($days as $day)
        {
            $appointments_tmp = $day->getAppointments();
            $appointments = array_merge($appointments, $day->getAppointments()->toArray());
        }

        return $appointments;
    }

    public function getDaysInTimeRange(DateTime $start, $duration)
    {
        $end     = $start->getTimestamp() + $duration;
        $endDate = new DateTime();
        $endDate ->setTimestamp($end);

        $days = array();

        foreach ($this->days as $day)
        {
            if ($day->getDate() < $start)
            {
                continue;
            }

            if ($day->getDate() >= $endDate)
            {
                break;
            }

            $days[] = $day;
        }

        return $days;
    }

    /**
     * @return boolean
     */
    public function hasAppointments()
    {
        foreach ($this->days as $day)
        {
            $appointments = $day->getAppointments();

            if (count($appointments) > 0)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * executes one part of argument checks for enlarge- & reduce-methods
     *
     * @param integer $direction
     *
     * @throws InvalidArgumentException
     */
    private function resizePeriodArgumentCheck($direction)
    {
        if ($direction != self::PERIOD_START || self::PERIOD_END)
        {
            throw new InvalidArgumentException();
        }
    }

    /**
     * @todo check for array pointer after unset
     *
     * @param DateTime $date
     */
    private function cutStart(DateTime $date)
    {
        $start           = $this->getStartDate($date);
        $currentDayIndex = key($this->days);

        foreach ($this->days as $key => $day)
        {
            if ($start < $key)
            {
                unset($this->days[$key]);
            }
            else
            {
                break;
            }
        }
    }

    /**
     * @param DateTime $date
     */
    private function addStart(DateTime $date)
    {
        $newStartDate = $this->getStartDate($date)->getTimestamp();

        reset($this->days);
        $firstDay  = key($this->days);
        $firstDate = $firstDay->getDate()->getTimestamp;

        for ($currentDate = $newStartDate; $currentDate < $firstDate; $currentDate += DateTimeHelper::TIME_DAY)
        {
            $tmpDate = new DateTime();
            $tmpDate -> setTimestamp($currentDate);

            $this->days[$currentDate] = new Day($tmpDate);
        }

        reset($this->days);
    }

    /**
     * @todo check for array pointer after unset
     *
     * @param DateTime $date
     */
    private function cutEnd(DateTime $date)
    {
        $end = $this->getStartDate($date);

        foreach ($this->days as $key => $day)
        {
            if ($end > $key)
            {
                unset($this->days[$key]);
            }
            else
            {
                break;
            }
        }
    }

    /**
     * @param DateTime $date
     */
    private function addEnd(DateTime $date)
    {
        $newEndDate = $this->getStartDate($date)->getTimestamp();

        end($this->days);
        $lastDay = key($this->days);
        $lastDate = $lastDay->getDate()->getTimestamp;

        for ($currentDate = $lastDate; $currentDate < $newEndDate; $currentDate += self::DAY)
        {
            $tmpDate = new DateTime();
            $tmpDate -> setTimestamp($currentDate);

            $this->days[$currentDate] = new Day($tmpDate);
        }

        end($this->days);
    }
}
