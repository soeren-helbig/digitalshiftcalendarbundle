<?php

namespace Digitalshift\CalendarBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class DigitalshiftCalendarExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        if (!isset($config['appointment_extensions'])) {
            throw new \InvalidArgumentException('The "appointment_extensions" option must be set');
        }

        if (!isset($config['download_extensions'])) {
            throw new \InvalidArgumentException('The "download_extensions" option must be set');
        }

        $container -> setParameter('digitalshift_calendar.appointment_extensions', $config['appointment_extensions']);
        $container -> setParameter('digitalshift_calendar.download_extensions', $config['download_extensions']);
        $container -> setParameter('digitalshift_calendar.views', $config['views']);

    }
}
