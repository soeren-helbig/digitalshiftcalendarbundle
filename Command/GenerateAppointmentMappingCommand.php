<?php

namespace Digitalshift\CalendarBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
// use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\ArrayInput;
// use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Yaml\Yaml;


/**
 * GenerateAppointmentMapping command to generate Doctrine Entity Mapping
 * after adding some appointment extensions
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class GenerateAppointmentMappingCommand extends ContainerAwareCommand
{
    const FILE_PATH = '../Resources/config/doctrine/Appointment.orm.yml';
    const APPOINTMENT_ENTITY = 'Digitalshift\CalendarBundle\Entity\Appointment';

    private $filePath, $extensions, $mapping, $output;

    public function __construct($name = null)
    {
        $this->filePath = __DIR__ . '/' . self::FILE_PATH;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('digitalshiftCalendar:generateAppointmentMapping')
            ->setDescription('Generate Doctrine Entity Mapping after added some Appointment extensions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $container = $this->getContainer();

        $this->extensions = $container->getParameter('digitalshift_calendar.appointment_extensions');
        $this->mapping    = $this->getMapping();

        $this->extendMapping();

        $this->backupExistingMapping();
        $this->writeExtendedMappingToFile();

         $this->generateDoctrineEntity();
    }

    /**
     * @return array
     */
    private function getMapping()
    {
        $fileContent = $this->readFileContent();

        return $this->parseMappingFile($fileContent);
    }

    /**
     * @return string
     */
    private function readFileContent()
    {
        $file = fopen($this->filePath, 'r');
        $content = fread($file, filesize($this->filePath));
        fclose($file);

        return $content;
    }

    /**
     * @param string $filePath
     * @param string $content
     * @param string $mode
     *
     * @throws \ErrorException
     */
    private function writeContentToFile($filePath, $content, $mode = 'w')
    {
        $file = fopen($filePath, $mode);
        if (strlen($content) !== fwrite($file, $content))
        {
            throw new \ErrorException();
        }

        fclose($file);
    }

    /**
     * @param string $fileContent
     * @return array
     */
    private function parseMappingFile($fileContent)
    {
        return Yaml::parse($fileContent);
    }

    /**
     * @todo hydrate initial key from current mapping
     *
     * @param array $mapping
     * @return array
     */
    private function extendMapping()
    {
        $this->output->writeln('Extending mapping: ');

        foreach($this->extensions as $extension)
        {
            $mappingId = $this->getMappedByIdentifier($extension);

            $mapping = array(
                'targetEntity' => $extension,
                'mappedBy'     => 'baseAppointment'
            );

            $this->mapping[self::APPOINTMENT_ENTITY]['oneToOne'][$mappingId] = $mapping;

            $this->output->writeln('  > '.$mappingId);
        }
    }

    /**
     * @param string $extension
     */
    private function getMappedByIdentifier($extension)
    {
        preg_match('/(\w)*$/', $extension, $matches);

        return lcfirst($matches[0]);
    }

    private function backupExistingMapping()
    {
        $this->output->write('Backing Up existing mapping ... ');

        $content = $this->readFileContent();
        $backupPath = $this->filePath . '~';

        $this->writeContentToFile($backupPath, $content);

        $this->output->writeln('[ OK ]');
    }

    private function writeExtendedMappingToFile()
    {
        $this->output->write('Writing extended mapping ... ');

        $content = Yaml::dump($this->mapping, 5);
        $this->writeContentToFile($this->filePath, $content);

        $this->output->writeln('[ OK ]');
    }

    private function generateDoctrineEntity()
    {
        $this->output->writeln('Regenerating base-Appointment ...');

        $arguments = new ArrayInput(
            array(
                'command' => 'doctrine:generate:entities',
                'name' => 'DigitalshiftCalendarBundle:Appointment'
            )
        );

        $command = $this->getApplication()->find('doctrine:generate:entities');
        $command->run($arguments, $this->output);
    }
}