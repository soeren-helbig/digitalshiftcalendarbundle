<?php

namespace Digitalshift\CalendarBundle\Command;

use Proxies\__CG__\Digitalshift\CalendarBundle\Entity\Appointment;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;


/**
 * GenerateAppointmentMapping command to generate Doctrine Entity Mapping
 * after adding some appointment extensions
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class GenerateDownloadMappingsCommand extends ContainerAwareCommand
{
    const MAPPING_BASE_PATH = '../Resources/config/doctrine/';
    const MAPPING_KEY = 'downloads';
    const BASE_NAMESPACE_ENTITIES = 'Digitalshift\\CalendarBundle\\Entity\\';

    private $mappingPath;

    /**
     * array (
     *     CalendarBundle-ENTITY => target-ENTITY (incl. namespace)
     *     'Holiday' => 'Digitalshift/MeditaBundle/Entity/Downloads'
     * )
     *
     * @var array
     */
    private $extensions;

    private $mappings;

    private $output;

    public function __construct($name = null)
    {
        $this->mappingPath = __DIR__ . '/' . self::MAPPING_BASE_PATH;
        $this->mappings = array();

        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('digitalshiftCalendar:generateDownloadMappings')
            ->setDescription('Generate Doctrine Entities Mapping after added some Download extensions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        $this->readExtension();
        $this->readMappings();

        $this->extendMappings();

        $this->backupExistingMappings();
        $this->writeExtendedMappingsToFile();

        $this->generateDoctrineEntities();
    }

    /**
     * @return void
     */
    private function readExtension()
    {
        $this->extensions = $this->getContainer()->getParameter('digitalshift_calendar.download_extensions');
    }

    /**
     * @return array
     */
    private function readMappings()
    {
        foreach ($this->extensions as $calendarBundleEntity => $targetEntity) {
            $fileContent = $this->readFileContent($calendarBundleEntity);
            $this->mappings[$calendarBundleEntity] = $this->parseMappingFile($fileContent);
        }
    }

    /**
     * @param string $entity
     * @return string
     */
    private function readFileContent($entity)
    {
        $filePath = $this->mappingPath . '/' . $entity . '.orm.yml';

        $file = fopen($filePath, 'r');
        $content = fread($file, filesize($filePath));
        fclose($file);

        return $content;
    }

    /**
     * @param string $fileContent
     * @return array
     */
    private function parseMappingFile($fileContent)
    {
        return Yaml::parse($fileContent);
    }

    /**
     * @return void
     */
    private function extendMappings()
    {
        $this->output->writeln("\n".'Extending mappings: ');

        foreach($this->extensions as $calendarBundleEntity => $targetEntity)
        {
            $this->extendMapping($calendarBundleEntity, $targetEntity);
        }
    }

    /**
     * @param string $calendarBundleEntity
     * @param string $targetEntity
     * @return void
     */
    private function extendMapping($calendarBundleEntity, $targetEntity)
    {
        $this->output->write(
            sprintf(
                '    %-\'.20s > %-\'.30s ',
                $calendarBundleEntity,
                $this->getEntityWithinNamespace($targetEntity)
            )
        );

        $mapping = array(
            'targetEntity' => $targetEntity,
            'inversedBy'   => lcfirst($calendarBundleEntity).'s',
            'joinTable'    => array(
                'name' => 'digitalshift_calendar_'.lcfirst($calendarBundleEntity).'_has_'.lcfirst($this->getEntityWithinNamespace($targetEntity)),
                'joinColumns' => array(
                    lcfirst($calendarBundleEntity).'_id' => array(
                        'referencedColumnName' => 'id'
                    )
                ),
                'inverseJoinColumns' => array(
                    lcfirst($this->getEntityWithinNamespace($targetEntity)).'_id' => array(
                        'referencedColumnName' => 'id'
                    )
                )
            )
        );

        $this->mappings[$calendarBundleEntity][self::BASE_NAMESPACE_ENTITIES.$calendarBundleEntity]['manyToMany'][self::MAPPING_KEY] = $mapping;

        $this->output->writeln('[OK]'."\n");
    }

    /**
     * @param string $namespace
     * @return string
     */
    private function getEntityWithinNamespace($namespace)
    {
        $elements = explode('\\', $namespace);

        return end($elements);
    }

    /**
     * @return void
     */
    private function backupExistingMappings()
    {
        $this->output->writeln('Backing Up existing mapping: ');

        foreach ($this->extensions as $calendarBundleEntity => $targetEntity) {

            $this->output->write(sprintf('    Entity: %-\'.20s', $calendarBundleEntity));

            $content = $this->readFileContent($calendarBundleEntity);
            $backupPath = $this->mappingPath . '/' . $calendarBundleEntity . '.orm.yml~';

            $this->writeContentToFile($backupPath, $content);
            $this->output->writeln('[ OK ]');
        }

    }

    /**
     * @param string $filePath
     * @param string $content
     * @param string $mode
     *
     * @throws \ErrorException
     */
    private function writeContentToFile($filePath, $content, $mode = 'w')
    {
        $file = fopen($filePath, $mode);
        if (strlen($content) !== fwrite($file, $content))
        {
            throw new \ErrorException();
        }

        fclose($file);
    }

    /**
     * @return void
     */
    private function writeExtendedMappingsToFile()
    {
        $this->output->writeln('Writing extended mapping: ');

        foreach ($this->mappings as $calendarBundleEntity => $mapping) {
            $this->output->write(sprintf('    Entity: %-\'.20s', $calendarBundleEntity));

            $content = Yaml::dump($mapping, 5);
            $filePath = $this->mappingPath . '/' . $calendarBundleEntity . '.orm.yml';

            $this->writeContentToFile($filePath, $content);

            $this->output->writeln('[ OK ]');
        }
    }

    /**
     * @todo: fix this!
     *
     * @return void
     */
    private function generateDoctrineEntities()
    {
        $this->output->writeln('Regenerating base-Appointment:');

        foreach ($this->extensions as $calendarBundleEntity => $targetEntity) {
            $this->output->writeln(sprintf('    Entity: %-\'.20s', $calendarBundleEntity));

            $arguments = new ArrayInput(
                array(
                    'command' => 'doctrine:generate:entities',
                    'name' => 'DigitalshiftCalendarBundle:'.$calendarBundleEntity
                )
            );

            $command = $this->getApplication()->find('doctrine:generate:entities');
            $command->run($arguments, $this->output);

            $this->output->writeln('[ OK ]');
        }
    }
}