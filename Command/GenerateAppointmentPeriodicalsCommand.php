<?php

namespace Digitalshift\CalendarBundle\Command;

use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Digitalshift\CalendarBundle\Periodicals\GeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * GenerateAppointmentPeriodicalsCommand to add all peridocal appointments to temporara table
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class GenerateAppointmentPeriodicalsCommand extends ContainerAwareCommand
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var GeneratorInterface;
     */
    private $periodicalGenerator;

    /**
     * @{inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('digitalshiftCalendar:generateAppointmentPeriodicals')
            ->setDescription('Generate AppointmentPeriodicals Doctrine Entities');
    }

    /**
     * @{inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $this->output              = $output;
        $this->entityManager       = $container->get('doctrine.orm.entity_manager');
        $this->periodicalGenerator = $container->get('digitalshift_calendar.periodicals.generator');

        $appointments = $this->getActivePeriodicalAppointments();
        $this->generatePeriodicals($appointments);
    }

    /**
     * retrieves all appointments which have an active period (not yot finished).
     */
    private function getActivePeriodicalAppointments()
    {
        return $this->entityManager->getRepository('DigitalshiftCalendarBundle:Appointment')->findByActivePeriod();
    }

    /**
     * @param array $appointments
     */
    private function generatePeriodicals($appointments = array())
    {
        /** @var AppointmentInterface $appointment */
        foreach ($appointments as $appointment) {
            $this->generatePeriodical($appointment);
        }
    }

    /**
     * @param AppointmentInterface $appointment
     */
    private function generatePeriodical(AppointmentInterface $appointment)
    {
        $this->output->write(
            sprintf(
                'Generating periodicals for appointment: %s ...',
                $appointment->getTitle()
            )
        );

        $this->periodicalGenerator->generate($this->entityManager, $appointment);

        $this->output->writeln(' DONE!');
    }
}