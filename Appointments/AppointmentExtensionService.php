<?php

namespace Digitalshift\CalendarBundle\Appointments;

use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;

/**
 * AppointmentExtension Service - provides some helper for automted retrieving
 * of appointment extension according to appointment.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class AppointmentExtensionService
{
    private $appointmentExtensions;

    /**
     * @param array $appointmentExtensions
     */
    public function __construct($appointmentExtensions)
    {
        $this->appointmentExtensions = $appointmentExtensions;
    }

    /**
     * @param AppointmentInterface $appointment
     */
    public function getExtensionName(AppointmentInterface $appointment)
    {
        $extension = $this->getExtension($appointment);

        return get_class($extension);
    }

    /**
     * @param AppointmentInterface $appointment
     */
    public function getExtension(AppointmentInterface $appointment)
    {
        foreach ($this->getMappingKeys() as $mappingKey)
        {
            $methodName = 'get'.ucfirst($mappingKey);
            $extension = $appointment->$methodName();

            if ($extension)
            {
                return $extension;
            }
        }
    }

    /**
     * @return string
     */
    public function getMappingKeys()
    {
        $mappingKeys = array();

        foreach ($this->appointmentExtensions as $extension)
        {
            $mappingKeys[] = $this->getMappingKey($extension);
        }

        return $mappingKeys;
    }

    /**
     * @param string $extensionClassName
     * @return string
     */
    public function getMappingKey($extensionClassName)
    {
        preg_match('/(\w)*$/', $extensionClassName, $matches);
        return lcfirst($matches[0]);
    }

    /**
     * @param mixed $extension
     * @return string
     */
    public function getExtensionClassName($extension)
    {
        return get_class($extension);
    }

    /**
     * @param mixed $extension
     */
    public function getTemplateNameForExtension($extension)
    {
        $extensionName = $this->getExtensionClassName($extension);
        return $extensionName;
    }
}