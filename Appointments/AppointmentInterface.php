<?php

namespace Digitalshift\CalendarBundle\Appointments;

/**
 * AppointmentIntrface for all appointment-entities;
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
abstract class AppointmentInterface
{
    public abstract function getId();

    public abstract function getStart();
    public abstract function setStart($start);

    public abstract function getEnd();
    public abstract function setEnd($end);

    public abstract function getTitle();
    public abstract function setTitle($title);
    public abstract function getDescription();
    public abstract function setDescription($description);

    public abstract function getPeriodicals();
    public abstract function addPeriodical(\Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical $periodicals);

    /**
     * @param \Digitalshift\CalendarBundle\Entity\AppointmentPeriods $period
     */
    public abstract function setPeriod(\Digitalshift\CalendarBundle\Entity\AppointmentPeriods $period = null);

    /**
     * @return \Digitalshift\CalendarBundle\Entity\AppointmentPeriods
     */
    public abstract function getPeriod();

    /**
     * @param AppointmentInterface $appointment1
     * @param AppointmentInterface $appointment2
     * @return int
     */
    public static function compare(AppointmentInterface $appointment1, AppointmentInterface $appointment2)
    {
        if ($appointment1->getStartTime() === $appointment2->getStartTime())
        {
            return 0;
        }

        return ($appointment1->getStartTime() > $appointment1->getStartTime()) ? +1 : -1;
    }

    /**
     * @param DateTime $start
     * @param DateTime $end
     * @param boolean $beforeStart
     * @param boolean $afterEnd
     */
    public function isValidInPeriod(\DateTime $start, \DateTime $end, $beforeStart = true, $afterEnd = true)
    {
        return ($this->getEnd() <= $start || $this->getStart() >= $end) ? false : true;
    }

}