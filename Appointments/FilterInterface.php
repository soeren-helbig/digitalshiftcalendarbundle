<?php

namespace Digitalshift\CalendarBundle\Appointments;

use Digitalshift\CalendarBundle\Appointments\AppointmentCollection;

/**
 * FilterInterface to filter appointment-entities.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
interface FilterInterface
{
    /**
     * add new filter criteria
     *
     * @param string $key
     * @param string $value
     */
    public function addCriteria($key, $value);

    /**
     * return filter criterias in valid selection-format
     *
     * @return string
     */
    public function getCriteriaForSelection();

    /**
     *@param AppointmentCollection $appointments - appointments to match against the settet filters
     */
    public function match(AppointmentCollection $appointments);
}