<?php

namespace Digitalshift\CalendarBundle\Appointments;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;
use InvalidArgumentException;

/**
 * AppointmentCollection a collection Appointments.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class AppointmentCollection implements Countable, IteratorAggregate, ArrayAccess
{
    private
        $elements;

    /**
     * @param array $elements - array of AppointmentInterface elements
     */
    public function __construct($elements = array())
    {
        // run type check
        foreach ($elements as $element)
        {
            if (get_class($element) !== 'DigitalshiftCalenderBundle\Appointments\AppointmentInterface')
            {
                throw new InvalidArgumentException();
            }
        }

        $this->elements = $elements;
    }

    /**
     * @param AppointmentInterface $appointment
     */
    public function add(AppointmentInterface $appointment)
    {
        $key = $this->getInsertionIndexForAppointment($appointment);
        $this->insertAppointmentAfterIndex($appointment, $key);
    }

    /**
     * runs through elements and returns the index by comparing start-times. the order
     * direction should be chronological.
     *
     * @param AppointmentInterface $appointment
     * @return int|string
     */
    private function getInsertionIndexForAppointment(AppointmentInterface $appointment)
    {
        $index = null;

        /** @var AppointmentInterface $indexAppointment */
        foreach ($this->elements as $key => $indexAppointment) {

            if ($indexAppointment->getStart() > $appointment->getStart()) {
                break;
            }

            $index = $key;

        }

        return $index;
    }

    /**
     * runs through elements until $index is found and adds $appointment after $index.
     * if index is not found, appointment will not be added.
     *
     * @param AppointmentInterface $appointment
     * @param $index
     */
    private function insertAppointmentAfterIndex(AppointmentInterface $appointment, $index)
    {
        $target = array();

        foreach ($this->elements as $key => $indexAppointment) {
            if ($key === $index) {
                $target[] = $indexAppointment;
                $target[] = $appointment;
            } else {
                $target[] = $indexAppointment;
            }
        }

        if (empty($this->elements)) {
            $target[] = $appointment;
        }

        $this->elements = $target;
    }

    /**
     * (non-PHPdoc)
     * @see Countable::count()
     *
     * @return integer
     */
    public function count()
    {
        return count($this->elements);
    }

    /**
     * (non-PHPdoc)
     * @see IteratorAggregate::getIterator()
     */
    public function getIterator()
    {
        return new ArrayIterator($this->elements);
    }

    /**
     * (non-PHPdoc)
     * @see ArrayAccess::offsetExists()
     *
     * @param integer $offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->lements);
    }

    /**
     * (non-PHPdoc)
     * @see ArrayAccess::offsetGet()
     *
     * @param integer $offset
     * @return AppointmentInterface
     *
     * @throws InvalidArgumentException
     */
    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset))
        {
            throw new InvalidArgumentException();
        }

        return $this->elements[$offset];
    }

    /**
     * (non-PHPdoc)
     * @see ArrayAccess::offsetSet()
     *
     * @param integer $offset
     * @param AppointmentInterface $element
     */
    public function offsetSet($offset, $element)
    {
        $this->elements[$offset];
    }

    /**
     * (non-PHPdoc)
     * @see ArrayAccess::offsetUnset()
     *
     * @param integer $offset
     *
     * @throws InvalidArgumentException
     */
    public function offsetUnset($offset)
    {
        if (!$this->offsetExists($offset))
        {
            throw new InvalidArgumentException();
        }

        unset($this->elements[$offset]);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->elements;
    }
}