<?php

namespace Digitalshift\CalendarBundle\Appointments;

use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Digitalshift\CalendarBundle\Appointments\FilterInterface;
use Doctrine\ORM\EntityManager;

/**
 * Crud-Controller for DigitalshiftCalender-Appointments
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class CrudController
{
    const REPOSITORY = 'DigitalshiftCalendarBundle:Appointment';

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param AppointmentInterface $appointment
     *
     * @throws \InvalidArgumentException
     */
    public function create(AppointmentInterface $appointment, $flush = false)
    {
        $this->entityManager->persist($appointment);

        if ($flush)
        {
            $this->entityManager->flush();
        }
    }

    /**
     * @param FilterInterface $filter
     * @return AppointmentInterface
     *
     * @throws \InvalidArgumentException
     */
    public function read($id)
    {
        $repository = $this->entityManager->getRepository(static::REPOSITORY);

        return $repository->find($id);
    }

    /**
     * @todo update instace vars
     *
     * @param integer $id
     * @param AppointmentInterface $appointment
     * @param boolean $flush
     */
    public function update($id, AppointmentInterface $appointment, $flush = true)
    {
        $repository  = $this->entityManager->getRepository(static::REPOSITORY);
        $appointment = $repository->find($id);

        // ...

        $this->entityManager->persist($appointment);

        if ($flush)
        {
            $this->entityManager->flush();
        }
    }

    /**
     * @param integer $id
     */
    public function delete($id)
    {
        $this->entityManager->remove($this->find($id));
    }
}