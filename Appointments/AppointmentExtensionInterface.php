<?php

namespace Digitalshift\CalendarBundle\Appointments;

use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;

/**
 * AppointmentExtensionInterface - interface for all appointment extensions.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
interface AppointmentExtensionInterface
{
    public function getViewBaseFolder();
}