<?php

namespace Digitalshift\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Digitalshift\CalendarBundle\Entity\AppointmentCategories
 */
class AppointmentCategories
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $appointments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->appointments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return AppointmentCategories
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add appointments
     *
     * @param Digitalshift\CalendarBundle\Entity\Appointment $appointments
     * @return AppointmentCategories
     */
    public function addAppointment(\Digitalshift\CalendarBundle\Entity\Appointment $appointments)
    {
        $this->appointments[] = $appointments;
    
        return $this;
    }

    /**
     * Remove appointments
     *
     * @param Digitalshift\CalendarBundle\Entity\Appointment $appointments
     */
    public function removeAppointment(\Digitalshift\CalendarBundle\Entity\Appointment $appointments)
    {
        $this->appointments->removeElement($appointments);
    }

    /**
     * Get appointments
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAppointments()
    {
        return $this->appointments;
    }
}