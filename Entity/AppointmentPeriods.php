<?php

namespace Digitalshift\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Digitalshift\CalendarBundle\Entity\AppointmentPeriods
 */
class AppointmentPeriods
{
    const TYPE_DAY = 0;
    const TYPE_WEEK = 1;
    const TYPE_MONTH = 2;
    const TYPE_YEAR = 4;

    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var integer $type
     */
    private $type;

    /**
     * @var integer $quantity
     */
    private $quantity;

    /**
     * @var \DateTime $end
     */
    private $end;

    /**
     * @var boolean
     */
    private $breakOnHolidays;

    /**
     * @var Digitalshift\CalendarBundle\Entity\Appointment
     */
    private $appointment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $breaks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->breaks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return AppointmentPeriods
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return AppointmentPeriods
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return AppointmentPeriods
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set breakOnHolidays
     *
     * @param boolean $breakOnHolidays
     * @return AppointmentPeriods
     */
    public function setBreakOnHolidays($breakOnHolidays)
    {
        $this->breakOnHolidays = $breakOnHolidays;

        return $this;
    }

    /**
     * Get breakOnHolidays
     *
     * @return boolean
     */
    public function getBreakOnHolidays()
    {
        return $this->breakOnHolidays;
    }

    /**
     * Set appointment
     *
     * @param Digitalshift\CalendarBundle\Entity\Appointment $appointment
     * @return AppointmentPeriods
     */
    public function setAppointment(\Digitalshift\CalendarBundle\Entity\Appointment $appointment = null)
    {
        $this->appointment = $appointment;

        return $this;
    }

    /**
     * Get appointment
     *
     * @return Digitalshift\CalendarBundle\Entity\Appointment
     */
    public function getAppointment()
    {
        return $this->appointment;
    }

    /**
     * Add breaks
     *
     * @param \Digitalshift\CalendarBundle\Entity\AppointmentPeriodBreaks $breaks
     * @return AppointmentPeriods
     */
    public function addBreak(\Digitalshift\CalendarBundle\Entity\AppointmentPeriodBreaks $breaks)
    {
        $this->breaks[] = $breaks;
    
        return $this;
    }

    /**
     * Remove breaks
     *
     * @param \Digitalshift\CalendarBundle\Entity\AppointmentPeriodBreaks $breaks
     */
    public function removeBreak(\Digitalshift\CalendarBundle\Entity\AppointmentPeriodBreaks $breaks)
    {
        $this->breaks->removeElement($breaks);
    }

    /**
     * Get breaks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBreaks()
    {
        return $this->breaks;
    }
}