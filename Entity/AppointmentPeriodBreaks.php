<?php

namespace Digitalshift\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Digitalshift\CalendarBundle\Library\DateTimeRange;

/**
 * AppointmentPeriodBreaks
 */
class AppointmentPeriodBreaks
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $end;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \Digitalshift\CalendarBundle\Entity\AppointmentPeriods
     */
    private $period;

    /**
     * decide if given TimeRange is part of the period break or not.
     *
     * @param array $timeRange
     * @return bool
     */
    public function isTimeRangeInBreak(DateTimeRange $timeRange)
    {
        return ($this->getStart() < $timeRange->getStart() && $this->getEnd() > $timeRange->getEnd())
            ? true
            : false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return AppointmentPeriodBreaks
     */
    public function setStart($start)
    {
        $this->start = $start;
    
        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return AppointmentPeriodBreaks
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return AppointmentPeriodBreaks
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set period
     *
     * @param \Digitalshift\CalendarBundle\Entity\AppointmentPeriods $period
     * @return AppointmentPeriodBreaks
     */
    public function setPeriod(\Digitalshift\CalendarBundle\Entity\AppointmentPeriods $period = null)
    {
        $this->period = $period;
    
        return $this;
    }

    /**
     * Get period
     *
     * @return \Digitalshift\CalendarBundle\Entity\AppointmentPeriods 
     */
    public function getPeriod()
    {
        return $this->period;
    }
}