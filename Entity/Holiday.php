<?php

namespace Digitalshift\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Digitalshift\CalendarBundle\Library\DateTimeRange;

/**
 * Holiday
 */
class Holiday
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $end;

    /**
     * @param $timeRange
     */
    public function includesTimeRange(DateTimeRange $timeRange)
    {
        return ($this->getStart() < $timeRange->getStart() && $this->getEnd() > $timeRange->getEnd())
            ? true
            : false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Holiday
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Holiday
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Holiday
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $downloads;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->downloads = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add downloads
     *
     * @param \Digitalshift\MeditaBundle\Entity\Downloads $downloads
     * @return Holiday
     */
    public function addDownload(\Digitalshift\MeditaBundle\Entity\Downloads $downloads)
    {
        $this->downloads[] = $downloads;

        return $this;
    }

    /**
     * Remove downloads
     *
     * @param \Digitalshift\MeditaBundle\Entity\Downloads $downloads
     */
    public function removeDownload(\Digitalshift\MeditaBundle\Entity\Downloads $downloads)
    {
        $this->downloads->removeElement($downloads);
    }

    /**
     * Get downloads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDownloads()
    {
        return $this->downloads;
    }
}
