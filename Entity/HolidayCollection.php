<?php

namespace Digitalshift\CalendarBundle\Entity;

use \DateTime;
use Digitalshift\CalendarBundle\Entity\Holiday;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * HolidayCollection
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class HolidayCollection extends ArrayCollection
{
    /**
     * @param DateTime $date
     * @return HolidayCollection
     */
    public function getByDate(DateTime $date)
    {
        $holidays = new HolidayCollection();

        /** @var Holiday $holiday */
        foreach ($this as $holiday) {
            if ($holiday->getStart()->format('Ymd') <= $date->format('Ymd') && $holiday->getEnd()->format('Ymd') >= $date->format('Ymd')) {
                $holidays->add($holiday);
            }
        }

        return $holidays;
    }
} 