<?php

namespace Digitalshift\CalendarBundle\Entity;

use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Digitalshift\CalendarBundle\Entity\Appointment
 */
class Appointment extends AppointmentInterface
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var Digitalshift\CalendarBundle\Entity\AppointmentCategories
     */
    private $categories;

    /**
     * @var \DateTime $start
     */
    private $start;

    /**
     * @var \DateTime $end
     */
    private $end;

    /**
     * @var Digitalshift\CalendarBundle\Entity\AppointmentPeriods
     */
    private $period;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $periodicals;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->periodicals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->getStart();
    }

    /**
     * @return integer
     */
    public function getDuration()
    {
        $start = $this->getStart()->getTimestamp();
        $end   = $this->getEnd()->getTimestamp();

        return $end - $start;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Appointment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Appointment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Appointment
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart($raw = false)
    {
        $start       = $this->start;
        $periodicals = $this->getPeriodicals();

        if ($periodicals->count() == 1 && !$raw)
        {
            $start = $periodicals->first()->getStart();
        }

        return $start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Appointment
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd($raw = false)
    {
        $end         = $this->end;
        $periodicals = $this->getPeriodicals();

        if ($periodicals->count() == 1 && !$raw)
        {
            $end = $periodicals->first()->getEnd();
        }

        return $end;
    }

    /**
     * Set categories
     *
     * @param Digitalshift\CalendarBundle\Entity\AppointmentCategories $categories
     * @return Appointment
     */
    public function setCategories(\Digitalshift\CalendarBundle\Entity\AppointmentCategories $categories = null)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get categories
     *
     * @return Digitalshift\CalendarBundle\Entity\AppointmentCategories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set period
     *
     * @param Digitalshift\CalendarBundle\Entity\AppointmentPeriods $period
     * @return Appointment
     */
    public function setPeriod(\Digitalshift\CalendarBundle\Entity\AppointmentPeriods $period = null)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return Digitalshift\CalendarBundle\Entity\AppointmentPeriods
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Add periodicals
     *
     * @param Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical $periodicals
     * @return Appointment
     */
    public function addPeriodical(\Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical $periodicals)
    {
        $this->periodicals[] = $periodicals;

        return $this;
    }

    /**
     * Remove periodicals
     *
     * @param Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical $periodicals
     */
    public function removePeriodical(\Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical $periodicals)
    {
        $this->periodicals->removeElement($periodicals);
    }

    /**
     * Get periodicals
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPeriodicals()
    {
        return $this->periodicals;
    }


    /**
     * @var \Digitalshift\MeditaBundle\Entity\CourseAppointment
     */
    private $courseAppointment;

    /**
     * @var \Digitalshift\MeditaBundle\Entity\WorkshopAppointment
     */
    private $workshopAppointment;


    /**
     * Set courseAppointment
     *
     * @param \Digitalshift\MeditaBundle\Entity\CourseAppointment $courseAppointment
     * @return Appointment
     */
    public function setCourseAppointment(\Digitalshift\MeditaBundle\Entity\CourseAppointment $courseAppointment = null)
    {
        $this->courseAppointment = $courseAppointment;

        return $this;
    }

    /**
     * Get courseAppointment
     *
     * @return \Digitalshift\MeditaBundle\Entity\CourseAppointment
     */
    public function getCourseAppointment()
    {
        return $this->courseAppointment;
    }

    /**
     * Set workshopAppointment
     *
     * @param \Digitalshift\MeditaBundle\Entity\WorkshopAppointment $workshopAppointment
     * @return Appointment
     */
    public function setWorkshopAppointment(\Digitalshift\MeditaBundle\Entity\WorkshopAppointment $workshopAppointment = null)
    {
        $this->workshopAppointment = $workshopAppointment;

        return $this;
    }

    /**
     * Get workshopAppointment
     *
     * @return \Digitalshift\MeditaBundle\Entity\WorkshopAppointment
     */
    public function getWorkshopAppointment()
    {
        return $this->workshopAppointment;
    }
}