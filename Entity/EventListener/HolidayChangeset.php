<?php

namespace Digitalshift\CalendarBundle\Entity\EventListener;

use DateTime;
use Digitalshift\CalendarBundle\Entity\Holiday;

/**
 * HolidayChangeset
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class HolidayChangeset
{
    /** @var DateTime */
    private $startOld;

    /** @var DateTime */
    private $startNew;

    /** @var DateTime */
    private $endOld;

    /** @var DateTime */
    private $endNew;

    public function __construct($changeset, Holiday $holiday)
    {
        $this->startOld = (isset($changeset['start']) && isset($changeset['start'][0])) ? $changeset['start'][0] : $holiday->getStart();
        $this->startNew = (isset($changeset['start']) && isset($changeset['start'][1])) ? $changeset['start'][1] : $holiday->getStart();
        $this->endOld   = (isset($changeset['end']) && isset($changeset['end'][0])) ? $changeset['end'][0] : $holiday->getEnd();
        $this->endNew   = (isset($changeset['end']) && isset($changeset['end'][1])) ? $changeset['end'][1] : $holiday->getEnd();
    }

    /**
     * @param DateTime $endNew
     * @return DateTime
     */
    public function setEndNew(Datetime $endNew)
    {
        $this->endNew = $endNew;

        return $this->endNew;
    }

    /**
     * @return DateTime
     */
    public function getEndNew()
    {
        return $this->endNew;
    }

    /**
     * @param DateTime $endOld
     * @return DateTime
     */
    public function setEndOld(DateTime $endOld)
    {
        $this->endOld = $endOld;

        return $this->endOld;
    }

    /**
     * @return DateTime
     */
    public function getEndOld()
    {
        return $this->endOld;
    }

    /**
     * @param DateTime $startNew
     * @return DateTime
     */
    public function setStartNew(DateTime $startNew)
    {
        $this->startNew = $startNew;

        return $this->startNew;
    }

    /**
     * @return DateTime
     */
    public function getStartNew()
    {
        return $this->startNew;
    }

    /**
     * @param DateTime $startOld
     * @return DateTime
     */
    public function setStartOld(DateTime $startOld)
    {
        $this->startOld = $startOld;

        return $this->startOld;
    }

    /**
     * @return DateTime
     */
    public function getStartOld()
    {
        return $this->startOld;
    }
} 