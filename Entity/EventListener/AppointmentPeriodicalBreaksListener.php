<?php

namespace Digitalshift\CalendarBundle\Entity\EventListener;

use Digitalshift\CalendarBundle\Entity\AppointmentPeriodBreaks;
use Digitalshift\CalendarBundle\Periodicals\GeneratorInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\EntityManager;

/**
 * AppointmentListener triggers periodical generation after save.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class AppointmentPeriodicalBreaksListener
{
    /**
     * @var GeneratorInterface
     */
    private $periodicalGenerator;

    /**
     * @param GeneratorInterface $generator
     */
    public function __construct(GeneratorInterface $generator)
    {
        $this->periodicalGenerator = $generator;
    }

    /**
     * event listener on flush persist
     *
     * @param LifecycleEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        /** @var \Doctrine\ORM\UnitOfWork $unityOfWork */
        $unityOfWork       = $args->getEntityManager()->getUnitOfWork();
        $insertions        = $unityOfWork->getScheduledEntityInsertions();
        $updates           = $unityOfWork->getScheduledEntityUpdates();
        $collectionUpdates = $unityOfWork->getScheduledCollectionUpdates();
        $deletions         = $unityOfWork->getScheduledEntityDeletions();

        $flushedEntities = array_merge(
            $insertions,
            $updates,
            $collectionUpdates,
            $deletions
        );

        foreach ($flushedEntities as $entity) {
            if ($entity instanceof AppointmentPeriodBreaks) {
                $this->updatePeriodicals($args->getEntityManager(), $entity);
            }
        }
    }

    /**
     * update periodicals if start/end date of appointment is changed and
     * appointment has a period.
     *
     * @param EntityManager $entityManager
     * @param AppointmentPeriodBreaks $appointmentPeriodBreaks
     */
    private function updatePeriodicals(EntityManager $entityManager, AppointmentPeriodBreaks $appointmentPeriodBreaks)
    {
        $changeset = $entityManager->getUnitOfWork()->getEntityChangeSet($appointmentPeriodBreaks);
        $deletions = $entityManager->getUnitOfWork()->getScheduledEntityDeletions();

        if (
            (isset($changeset['start']) || isset($changeset['end'])) &&
            $appointmentPeriodBreaks->getPeriod()
        ) {
            $this->periodicalGenerator->generateWithNotFlushedBreak(
                $entityManager,
                $appointmentPeriodBreaks->getPeriod()->getAppointment(),
                $appointmentPeriodBreaks,
                true
            );
        }

        if ($deletions) {
            $this->periodicalGenerator->generateWithNotFlushedBreak(
                $entityManager,
                $appointmentPeriodBreaks->getPeriod()->getAppointment(),
                $appointmentPeriodBreaks,
                true,
                true
            );
        }
    }
}