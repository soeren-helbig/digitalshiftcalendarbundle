<?php

namespace Digitalshift\CalendarBundle\Entity\EventListener;

use Digitalshift\CalendarBundle\Entity\Holiday;
use Digitalshift\CalendarBundle\Periodicals\UpdaterInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\EntityManager;

/**
 * HolidayListener
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class HolidayListener
{
    /**
     * @var UpdaterInterface
     */
    private $periodicalUpdater;

    /**
     * @param GeneratorInterface $generator
     */
    public function __construct(UpdaterInterface $updater)
    {
        $this->periodicalUpdater = $updater;
    }

    /**
     * event listener on flush persist
     *
     * @param LifecycleEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        /** @var \Doctrine\ORM\UnitOfWork $unityOfWork */
        $unityOfWork       = $args->getEntityManager()->getUnitOfWork();
        $insertions        = $unityOfWork->getScheduledEntityInsertions();
        $updates           = $unityOfWork->getScheduledEntityUpdates();
        $collectionUpdates = $unityOfWork->getScheduledCollectionUpdates();
        $deletions         = $unityOfWork->getScheduledEntityDeletions();

        $flushedEntities = array_merge(
            $insertions,
            $updates,
            $collectionUpdates
        );

        foreach ($flushedEntities as $entity) {
            if ($entity instanceof Holiday) {
                $this->updatePeriodicals($args->getEntityManager(), $entity);
            }
        }

        // special handling for deletions (no changeset given)
        foreach ($deletions as $entity) {
            if ($entity instanceof Holiday) {
                $this->removeBreaksForDeletion($args->getEntityManager(), $entity);
            }
        }
    }

    /**
     * @param EntityManager $entityManager
     * @param Holiday $holiday
     */
    public function updatePeriodicals(EntityManager $entityManager, Holiday $holiday)
    {
        $changeset = $entityManager->getUnitOfWork()->getEntityChangeSet($holiday);

        if (isset($changeset['start']) || isset($changeset['end'])) {
            $this->periodicalUpdater->cascadeHolidayUpdate(
                $entityManager,
                new HolidayChangeset($changeset, $holiday),
                true
            );
        }
    }

    /**
     * @param EntityManager $entityManager
     * @param Holiday $holiday
     */
    public function removeBreaksForDeletion(EntityManager $entityManager, Holiday $holiday)
    {
        $this->periodicalUpdater->removeHolidayBreaks(
            $entityManager,
            $holiday,
            true
        );
    }
}