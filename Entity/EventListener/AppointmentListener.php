<?php

namespace Digitalshift\CalendarBundle\Entity\EventListener;

use Digitalshift\CalendarBundle\Entity\Appointment;
use Digitalshift\CalendarBundle\Periodicals\GeneratorInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\EntityManager;

/**
 * AppointmentListener triggers periodical generation after save.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class AppointmentListener
{
    /**
     * @var GeneratorInterface
     */
    private $periodicalGenerator;

    /**
     * @param GeneratorInterface $generator
     */
    public function __construct(GeneratorInterface $generator)
    {
        $this->periodicalGenerator = $generator;
    }

    /**
     * event listener on flush persist
     *
     * @param LifecycleEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        /** @var \Doctrine\ORM\UnitOfWork $unityOfWork */
        $unityOfWork       = $args->getEntityManager()->getUnitOfWork();
        $insertions        = $unityOfWork->getScheduledEntityInsertions();
        $updates           = $unityOfWork->getScheduledEntityUpdates();
        $collectionUpdates = $unityOfWork->getScheduledCollectionUpdates();

        $flushedEntities = array_merge(
            $insertions,
            $updates,
            $collectionUpdates
        );

        foreach ($flushedEntities as $entity) {
            if ($entity instanceof Appointment) {
                $this->updatePeriodicals($args->getEntityManager(), $entity);
            }
        }
    }

    /**
     * update periodicals if start/end date of appointment is changed and
     * appointment has a period.
     *
     * @param EntityManager $entityManager
     * @param Appointment $appointment
     */
    private function updatePeriodicals(EntityManager $entityManager, Appointment $appointment)
    {
        $changeset = $entityManager->getUnitOfWork()->getEntityChangeSet($appointment);

        if (
            (isset($changeset['start']) || isset($changeset['end'])) &&
            $appointment->getPeriod()
        ) {
            $this->periodicalGenerator->generate($entityManager, $appointment, true);
        }
    }
}