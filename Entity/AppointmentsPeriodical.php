<?php

namespace Digitalshift\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical
 */
class AppointmentsPeriodical
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var \DateTime $start
     */
    private $start;

    /**
     * @var \DateTime $end
     */
    private $end;

    /**
     * @var boolean
     */
    private $inBreak;

    /**
     * @var Digitalshift\CalendarBundle\Entity\Appointments
     */
    private $appointment;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return AppointmentsPeriodical
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return AppointmentsPeriodical
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set inBreak
     *
     * @param boolean $inBreak
     * @return AppointmentsPeriodical
     */
    public function setInBreak($inBreak)
    {
        $this->inBreak = $inBreak;

        return $this;
    }

    /**
     * Get inBreak
     *
     * @return boolean
     */
    public function getInBreak()
    {
        return $this->inBreak;
    }

    /**
     * Set appointment
     *
     * @param Digitalshift\CalendarBundle\Entity\Appointment $appointment
     * @return AppointmentsPeriodical
     */
    public function setAppointment(\Digitalshift\CalendarBundle\Entity\Appointment $appointment = null)
    {
        $this->appointment = $appointment;

        return $this;
    }

    /**
     * Get appointment
     *
     * @return Digitalshift\CalendarBundle\Entity\Appointment
     */
    public function getAppointment()
    {
        return $this->appointment;
    }

}