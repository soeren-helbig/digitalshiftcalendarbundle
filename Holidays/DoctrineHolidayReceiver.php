<?php

namespace Digitalshift\CalendarBundle\Holidays;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Digitalshift\CalendarBundle\Entity\HolidayCollection;

/**
 * DoctrineHolidayReciever
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class DoctrineHolidayReceiver implements HolidayReceiverInterface
{
    const REPOSITORY = 'DigitalshiftCalendarBundle:Holiday';

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @{inheritdoc}
     */
    public function fetchByDate(DateTime $start, DateTime $end = null)
    {
        $holidays = $this->entityManager
            ->getRepository(static::REPOSITORY)
            ->findByTimeRange($start, $end);

        return ($holidays) ? new HolidayCollection($holidays) : null;
    }
}