<?php

namespace Digitalshift\CalendarBundle\Holidays;

use \DateTime;
use Digitalshift\CalendarBundle\Entity\Holiday;
use Doctrine\ORM\EntityManagerInterface;

/**
 * HolidayReceiverInterface
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
interface HolidayReceiverInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager);

    /**
     * @param DateTime $start
     * @param DateTime $end
     * @return Holiday
     */
    public function fetchByDate(DateTime $start, DateTime $end = null);
} 