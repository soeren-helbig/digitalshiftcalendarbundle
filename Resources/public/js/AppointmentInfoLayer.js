/**
 * @author Soeren Helbig <soeren.helbig@digitalshift.de>
 * @copyright Digitalshift
 */
var AppointmentInfoLayer = function (params) {

    this.params = {};

    $.extend(this.params, {
        'url'               : '/calendar/appointmentInfo',
        'wrapper'           : null,
        'loadingIndicator'  : '/bundles/digitalshiftcalendar/images/ajax-loader.gif',
        'appointments'      : '.calendar_item_wrapper',
        'width'             : 527,
        'maxHeight'         : 550
    }, params);

    this.activeAppointment = null;
    this.appointmentLoadingIndicator = {
        'id' : 'appointmentLoadingIndicator'
    };

    this.init = function () {

        var that = this;
        var initWrapper, initDialog, addEventHandlers, addOpenOnClickHandler, addOpenOnHoverHandler;

        initWrapper = function () {
            if (!that.params.wrapper || $(this.params.wrapper).length === 0) {
                that.params.wrapper = document.createElement('div');
                $('body').append(that.params.wrapper);
            }
        };

        initDialog = function () {
            $(that.params.wrapper).dialog({
                'width'     : that.params.width,
                'height'    : that.params.height,
                'maxHeight' : that.params.maxHeight,
                'autoOpen'  : false,
                'modal'     : true,
                'resizable' : false,
                'draggable' : false
            });
        };

        addEventHandlers = function () {
            $(that.params.appointments).each(function (index, element) {
                addOpenOnClickHandler(element);
            });
        };

        addOpenOnClickHandler = function(element) {
            $(element).click(function (event) {
                that.activeAppointment = event.currentTarget;
                var appointmentId      = $(event.currentTarget).data('id');
                that.open(appointmentId);
            });
        };

        initWrapper();
        initDialog();
        addEventHandlers();
    };

    /**
     * @param integer appointmentId
     * @param function successCallback
     */
    this.loadAppointmentInformation = function (appointmentId, successCallback) {

        var that = this;

        $.ajax(
            this.params.url + '/' + appointmentId,
            {
                dataType : 'json',
                success  : function (data, textStatus, xhr) {
                    if (typeof successCallback === 'function') {
                        successCallback.call(that, data);
                    }
                },
                complete : function () {
                    that.hideLoadingIndicator.call(that);
                }
            }
        );
    };

    /**
     * @param string informationMarkup
     */
    this.addAppointmentInformationToWrapper = function (informationMarkup) {
        $(this.params.wrapper).empty().append(informationMarkup);
    };

    /**
     * 
     */
    this.showLoadingIndicator = function () {

        var loadingIndicator, layer;

        loadingIndicator = document.createElement('img');
        $(loadingIndicator)
            .attr('src', this.params.loadingIndicator);

        layer = document.createElement('div');
        $(layer)
            .attr('id', this.appointmentLoadingIndicator.id)
            .addClass('loading_indicator')
            .append(loadingIndicator);

        $(this.activeAppointment).append(layer);
    };

    /**
     * 
     */
    this.hideLoadingIndicator = function () {
        $('#' + this.appointmentLoadingIndicator.id).remove();
    };

    this.init();
};

/**
 * @param integer appointmentId
 */
AppointmentInfoLayer.prototype.open = function (appointmentId) {
    this.showLoadingIndicator();

    this.loadAppointmentInformation(appointmentId, function (data) {
        this.addAppointmentInformationToWrapper(data.html);
        $(this.params.wrapper).dialog('open');
    });
};

/**
 * @param integer appointmentId
 */
AppointmentInfoLayer.prototype.close = function (appointmentId) {
    $(this.params.wrapper).dialog('close');
};