/**
 * @author Soeren Helbig <soeren.helbig@digitalshift.de>
 * @copyright Digitalshift
 */
// prepare namespace
var digitalshift = digitalshift || {};
digitalshift.calendar = digitalshift.calendar || {};

/**
 * @author Soeren Helbig <soeren.helbig@digitalshift.de>
 * @copyright Digitalshift
 */
digitalshift.calendar.AppointmentInfoTooltip = function (layerParams) {
    var params;

    // prepare members
    params = {};
    $.extend(params, {
        listWrapper : 'table.calendar',
        appointments : '.item'
    }, layerParams);

    /**
     * @constructor
     */
    (function () {
        var getTooltipParams, getTooltipContent, tooltipParams;

        /**
         * @returns {Object}
         */
        getTooltipParams = function() {
            var tooltipParams = {};

            tooltipParams.items = params.appointments;
            tooltipParams.content = getTooltipContent;
            tooltipParams.position = {
                my: 'left+30 center',
                at: 'right center'
            }

            return tooltipParams;
        };

        /**
         * @returns {*|jQuery}
         */
        getTooltipContent = function() {
            var domAppointment = $(this);

            return $(domAppointment).find('.calendar-tooltip').html();
        };

        tooltipParams = getTooltipParams();
        $(params.listWrapper).tooltip(tooltipParams);
    }());
};
