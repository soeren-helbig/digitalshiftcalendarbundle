<?php

namespace Digitalshift\CalendarBundle\Periodicals;

use Digitalshift\CalendarBundle\Entity\EventListener\HolidayChangeset;
use Digitalshift\CalendarBundle\Entity\Holiday;
use Doctrine\ORM\EntityManager;

/**
 * UpdateInterface for updateting periodicals according to holiday update.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
interface UpdaterInterface
{
    /**
     * @param EntityManager $entityManager
     * @param HolidayChangeset $changeset
     * @param bool $isDoctrineEvent
     * @return mixed
     */
    public function cascadeHolidayUpdate(EntityManager $entityManager, HolidayChangeset $changeset, $isDoctrineEvent = false);

    /**
     * @param EntityManager $entityManager
     * @param Holiday $holiday
     * @return mixed
     */
    public function removeHolidayBreaks(EntityManager $entityManager, Holiday $holiday, $isDoctrineEvent = false);
}