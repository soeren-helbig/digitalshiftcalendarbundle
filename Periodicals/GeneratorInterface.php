<?php

namespace Digitalshift\CalendarBundle\Periodicals;

use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Digitalshift\CalendarBundle\Entity\AppointmentPeriodBreaks;
use Digitalshift\CalendarBundle\Entity\EventListener\HolidayChangeset;
use Doctrine\ORM\EntityManager;

/**
 * GeneratorInterface for generating periodicals according to appointment
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
interface GeneratorInterface
{
    /**
     * @param EntityManager $entityManager
     * @param AppointmentInterface $appointment
     * @param Boolean $isDoctrineEvent
     * @return mixed
     */
    public function generate(EntityManager $entityManager, AppointmentInterface $appointment, $isDoctrineEvent = false);

    /**
     * @param EntityManager $entityManager
     * @param AppointmentInterface $appointment
     * @param AppointmentPeriodBreaks $appointmentPeriodBreak
     * @param bool $isDoctrineEvent
     * @param bool $isDeleted
     * @return mixed
     */
    public function generateWithNotFlushedBreak(
        EntityManager $entityManager,
        AppointmentInterface $appointment,
        AppointmentPeriodBreaks $appointmentPeriodBreak,
        $isDoctrineEvent = false,
        $isDeleted = false
    );
}