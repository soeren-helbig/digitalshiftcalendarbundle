<?php

namespace Digitalshift\CalendarBundle\Periodicals;

use DateTime;
use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Digitalshift\CalendarBundle\Entity\AppointmentPeriods;
use Digitalshift\CalendarBundle\Entity\AppointmentPeriodBreaks;
use Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical;
use Digitalshift\CalendarBundle\Library\DateTimeRange;
use Doctrine\ORM\EntityManager;

/**
 * Generator for gernerating periodicals according to appointment
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class Generator extends BaseEventListener implements GeneratorInterface
{
    /** @var boolean */
    private $isDoctrineEvent;

    /** @var AppointmentPeriodBreaks */
    private $notFlushedBreak;

    /** @var Boolean */
    private $notFlushedBreakDeleted;

    /**
     * @{inheritdoc}
     * */
    public function generate(EntityManager $entityManager, AppointmentInterface $appointment, $isDoctrineEvent = false)
    {
        if (!$appointment->getPeriod()) {
            return;
        }

        $this->entityManager   = $entityManager;
        $this->isDoctrineEvent = $isDoctrineEvent;

        $this->cleanUpPeriodicals($appointment);
        $this->createNewPeriodicals($appointment);

        if (!$this->isDoctrineEvent) {
            $this->entityManager->flush();
        }
    }

    public function generateWithNotFlushedBreak(
        EntityManager $entityManager,
        AppointmentInterface $appointment,
        AppointmentPeriodBreaks $appointmentPeriodBreak,
        $isDoctrineEvent = false,
        $isDeleted = false
    ) {
        $this->notFlushedBreak = $appointmentPeriodBreak;
        $this->notFlushedBreakDeleted = $isDeleted;
        $this->generate($entityManager, $appointment, $isDoctrineEvent);
    }

    /**
     * cleanup current periodical-entries for Appointment
     *
     * @param AppointmentInterface $appointment
     */
    private function cleanUpPeriodicals(AppointmentInterface $appointment)
    {
        $periodicals = $appointment->getPeriodicals();

        /** @var AppointmentsPeriodical */
        foreach ($periodicals as $periodical) {
            $this->entityManager->remove($periodical);

            if ($this->isDoctrineEvent) {
                $this->computeChangeSet($periodical);
            }
        }
    }

    /**
     * generate new periodical entries for Appointment
     *
     * @param AppointmentInterface $appointment
     */
    private function createNewPeriodicals(AppointmentInterface $appointment)
    {
        $periodTimeRanges = $this->getPeriodTimeRangesForAppointment($appointment);

        /** @var DateTimeRange $timeRange */
        foreach ($periodTimeRanges as $timeRange) {
            $inBreak = $this->isTimeRangeInBreak($appointment, $timeRange);
            $this->addPeriodical(
                $timeRange->getStart(),
                $timeRange->getEnd(),
                $inBreak,
                $appointment
            );
        }
    }

    /**
     * @todo make other ways than weekly period possible => AppointmentPeriodicalService
     *
     * @param AppointmentInterface $appointment
     * @throws \Exception
     */
    private function getPeriodTimeRangesForAppointment(AppointmentInterface $appointment)
    {
        if ($appointment->getPeriod()->getType() != AppointmentPeriods::TYPE_WEEK || $appointment->getPeriod()->getQuantity() != 1)
        {
            throw new \Exception('ONLY 1x WEEKLY PERIODS ALLOWED');
        }

        $start    = $appointment->getStart(true);
        $periods  = array();
        $interval = new \DateInterval('P1W');

        foreach (new \DatePeriod($start, $interval, $appointment->getPeriod()->getEnd()) as $date)
        {
            $start = $date;

            $end = new DateTime();
            $end->setTimestamp($start->getTimestamp() + $appointment->getDuration());

            $periods[] = new DateTimeRange($start, $end);
        }

        return $periods;
    }

    /**
     * @param AppointmentInterface $appointment
     * @param DateTimeRange $timeRange
     * @return boolean
     */
    private function isTimeRangeInBreak(AppointmentInterface $appointment, DateTimeRange $timeRange)
    {
        $inPeriodBreak = $this->isInPeriodBreak($appointment, $timeRange);
        $inHoliday     = ($appointment->getPeriod()->getBreakOnHolidays() && $this->isInHoliday($timeRange));

        return ($inPeriodBreak || $inHoliday);
    }

    /**
     * @param AppointmentInterface $appointment
     * @param DateTimeRange $timeRange
     * @return bool
     */
    private function isInPeriodBreak(AppointmentInterface $appointment, DateTimeRange $timeRange)
    {
        $breaks = $appointment->getPeriod()->getBreaks();

        /** @var $break \Digitalshift\CalendarBundle\Entity\AppointmentPeriodBreaks */
        foreach ($breaks as $break) {
            if ($break == $this->notFlushedBreak && $this->notFlushedBreakDeleted) {
                continue;
            }

            if ($break->isTimeRangeInBreak($timeRange)) {
                return true;
            }
        }

        if ($this->notFlushedBreak && $this->notFlushedBreak->isTimeRangeInBreak($timeRange) && !$this->notFlushedBreakDeleted) {
            return true;
        }

        return false;
    }

    /**
     * @param DateTimeRange $timeRange
     */
    private function isInHoliday(DateTimeRange $timeRange)
    {
        $holidays = $this->entityManager->getRepository('DigitalshiftCalendarBundle:Holiday')->findAll();

        /** @var $holiday \Digitalshift\CalendarBundle\Entity\Holiday */
        foreach ($holidays as $holiday) {
            if ($holiday->includesTimeRange($timeRange)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param DateTime $start
     * @param DateTime $end
     * @param boolean $inBreak
     * @param AppointmentInterface $appointment
     */
    private function addPeriodical(DateTime $start, DateTime $end, $inBreak, AppointmentInterface $appointment)
    {
        $periodical = new AppointmentsPeriodical();
        $periodical -> setStart($start);
        $periodical -> setEnd($end);
        $periodical -> setAppointment($appointment);
        $periodical -> setInBreak($inBreak);

        $appointment->addPeriodical($periodical);

        $this->entityManager->persist($periodical);

        if ($this->isDoctrineEvent) {
            $this->computeChangeSet($periodical);
        }

        $this->entityManager->persist($appointment);
    }

}