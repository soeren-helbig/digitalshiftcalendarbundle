<?php

namespace Digitalshift\CalendarBundle\Periodicals;

use Digitalshift\CalendarBundle\Entity\EventListener\HolidayChangeset;
use Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical;
use Digitalshift\CalendarBundle\Entity\Holiday;
use Digitalshift\CalendarBundle\Library\DateTimeRange;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Updater
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class Updater extends BaseEventListener implements UpdaterInterface
{
    const PERIODICAL_ENTITY_REPOSITORY = 'DigitalshiftCalendarBundle:AppointmentsPeriodical';

    /** @var boolean */
    private $isDoctrineEvent;

    /**
     * @{inheritdoc}
     */
    public function cascadeHolidayUpdate(EntityManager $entityManager, HolidayChangeset $changeset, $isDoctrineEvent = false)
    {
        $this->isDoctrineEvent = $isDoctrineEvent;
        $this->entityManager = $entityManager;

        $this->removeObsoleteBreaks($entityManager, $changeset);
        $this->addNewBreaks($entityManager, $changeset);

        if (!$this->isDoctrineEvent) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param EntityManager $entityManager
     * @param HolidayChangeset $changeset
     */
    private function removeObsoleteBreaks(EntityManager $entityManager, HolidayChangeset $changeset)
    {
        $periodicals = $entityManager
            ->getRepository(self::PERIODICAL_ENTITY_REPOSITORY)
            ->findObsoleteBreaksForHolidayChange($changeset);

        $this->removeBreakForPeriodicals($periodicals);
    }

    /**
     * @param array $periodicals
     */
    private function removeBreakForPeriodicals($periodicals)
    {
        /** @var AppointmentsPeriodical $periodical */
        foreach ($periodicals as $periodical) {
            $periodical->setInBreak(false);

            if ($this->isDoctrineEvent) {
                $this->computeChangeSet($periodical);
            }
        }
    }

    /**
     * @param EntityManager $entityManager
     * @param HolidayChangeset $changeset
     */
    private function addNewBreaks(EntityManager $entityManager, HolidayChangeset $changeset)
    {
        $periodicals = $entityManager
            ->getRepository(self::PERIODICAL_ENTITY_REPOSITORY)
            ->findNewBreakesForHolidayChange($changeset);

        $this->addBreakForPeriodicals($periodicals);
    }

    /**
     * @param array $periodicals
     */
    private function addBreakForPeriodicals(array $periodicals)
    {
        /** @var AppointmentsPeriodical $periodical */
        foreach ($periodicals as $periodical) {
            $periodical->setInBreak(true);

            if ($this->isDoctrineEvent) {
                $this->computeChangeSet($periodical);
            }
        }
    }

    /**
     * @param EntityManager $entityManager
     * @param Holiday $holiday
     */
    public function removeHolidayBreaks(EntityManager $entityManager, Holiday $holiday, $isDoctrineEvent = false)
    {
        $this->isDoctrineEvent = $isDoctrineEvent;
        $this->entityManager = $entityManager;

        $periodicals = $entityManager
            ->getRepository(self::PERIODICAL_ENTITY_REPOSITORY)
            ->findInDateTimeRange(
                new DateTimeRange(
                    $holiday->getStart(),
                    $holiday->getEnd()
                ),
                true,
                true
            );

        $this->removeBreakForPeriodicals($periodicals);

        if (!$this->isDoctrineEvent) {
            $this->entityManager->flush();
        }
    }
} 