<?php

namespace Digitalshift\CalendarBundle\Periodicals;

use Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical;
use Doctrine\ORM\EntityManager;

/**
 * BaseEventListener
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class BaseEventListener
{
    const CLASS_METADATA_APPOINTMENT_PERDIODICALS = 'Digitalshift\CalendarBundle\Entity\AppointmentsPeriodical';

    /** @var EntityManager */
    protected $entityManager;

    /**
     * @param AppointmentsPeriodical $periodical
     */
    protected function computeChangeSet(AppointmentsPeriodical $periodical)
    {
        $this->entityManager->getUnitOfWork()->computeChangeSet(
            $this->entityManager->getClassMetadata(self::CLASS_METADATA_APPOINTMENT_PERDIODICALS),
            $periodical
        );
    }
} 