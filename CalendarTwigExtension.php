<?php

namespace Digitalshift\CalendarBundle;

use DateTime;
use Digitalshift\CalendarBundle\Appointments\AppointmentInterface;
use Digitalshift\CalendarBundle\Appointments\AppointmentExtensionService;
use Digitalshift\CalendarBundle\AppointmentWrappers\Month;
use Digitalshift\CalendarBundle\AppointmentWrappers\Week;
use Digitalshift\CalendarBundle\AppointmentWrappers\DayCollection;
use Digitalshift\CalendarBundle\Entity\Holiday;
use Digitalshift\CalendarBundle\ViewTransformer\Week\WeeklyViewTransformer;
use Digitalshift\CalendarBundle\ViewTransformer\Month\MonthlyViewTransformer;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

/**
 * Calender TwigExtension - provides some helper for viewRendering
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class CalendarTwigExtension extends \Twig_Extension
{
    private $weeklyViewTransformer, $viewFolder, $appointmentExtensionService, $environment, $appointment;

    /**
     * @param MonthlyViewTransformer $monthlyViewTransformer
     * @param WeeklyViewTransformer $weeklyViewTransformer
     * @param string $viewFolder
     * @param AppointmentExtensionService $appointmentExtensionService
     */
    public function __construct(
        MonthlyViewTransformer $monthlyViewTransformer,
        WeeklyViewTransformer $weeklyViewTransformer,
        $viewFolder = null,
        AppointmentExtensionService $appointmentExtensionService = null
    ) {
        $this->monthlyViewTransformer      = $monthlyViewTransformer;
        $this->weeklyViewTransformer       = $weeklyViewTransformer;
        $this->viewFolder                  = $viewFolder;
        $this->appointmentExtensionService = $appointmentExtensionService;
        $this->environment                 = null;
        $this->appointment                 = null;
    }

    /**
     * (non-PHPdoc)
     * @see Twig_Extension::getFilters()
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            'mynospace' => new \Twig_Filter_Method($this, 'noSpace', array('is_safe' => array('html')))
        );
    }

    /**
     * (non-PHPdoc)
     * @see Twig_Extension::getFunctions()
     *
     * @return array
     */
    function getFunctions()
    {
        $options = array(
            'needs_environment' => true,
            'is_safe' => array('html')
        );

        return array(
            'appointment_info'           => new \Twig_Function_Method($this, 'appointmentInfo'         , $options),
            'monthly_view'               => new \Twig_Function_Method($this, 'monthlyView'             , $options),
            'list_view'                  => new \Twig_Function_Method($this, 'listView'                , $options),
            'render_weekly_appointment'  => new \Twig_Function_Method($this, 'renderWeeklyAppointment' , $options),
            'render_monthly_appointment' => new \Twig_Function_Method($this, 'renderMonthlyAppointment', $options),
            'render_list_appointment'    => new \Twig_Function_Method($this, 'renderListAppointment'   , $options),
            'render_list_holiday'        => new \Twig_Function_Method($this, 'renderListHoliday'       , $options),
            'weekly_view'                => new \Twig_Function_Method($this, 'weeklyView'              , $options)
        );
    }

    /**
     * (non-PHPdoc)
     * @see Twig_ExtensionInterface::getName()
     *
     * @return string
     */
    public function getName()
    {
        return 'digitalshift_calendar';
    }

    /**
     * @param string $theString
     * @return mixed
     */
    public function noSpace($theString)
    {
        return str_replace(' ', '&nbsp;', $theString);
    }

    /**
     * @param DayCollection $dayCollection
     * @param integer $defaultRangeLength
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    public function weeklyView(\Twig_Environment $environment, Week $week, $defaultRangeLength = 1800, DateTime $startTime = null, DateTime $endTime = null)
    {
        $startTime = ($startTime) ? $startTime : new DateTime('00:00:00');
        $endTime   = ($endTime)   ? $endTime   : new DateTime('23:59:59');

        return $environment->render(
            'DigitalshiftCalendarBundle:Weekly:index.html.twig',
            array(
                'dayTimes' => $this->weeklyViewTransformer->getView($week, $defaultRangeLength, $startTime, $endTime)
            )
        );
    }

    /**
     * @param \Twig_Environment $environment
     * @param Month $month
     */
    public function monthlyView(\Twig_Environment $environment, Month $month)
    {
        return $environment->render(
            'DigitalshiftCalendarBundle:Monthly:index.html.twig',
            array(
                'weeks' => $this->monthlyViewTransformer->getView($month)
            )
        );
    }

    /**
     * @param \Twig_Environment $environment
     * @param DayCollection $days
     */
    public function listView(\Twig_Environment $environment, DayCollection $days, $layer = true, $tooltip = false)
    {
        return $environment->render(
            'DigitalshiftCalendarBundle:List:index.html.twig',
            array(
                'days' => $days,
                'layer' => $layer,
                'tooltip' => $tooltip
            )
        );
    }

    /**
     * @param \Twig_Environment $environment
     * @param AppointmentInterface $appointment
     * @return string
     */
    public function renderWeeklyAppointment(\Twig_Environment $environment, AppointmentInterface $appointment)
    {
        $this->environment = $environment;
        $this->appointment = $appointment;

        $defaultTemplate = 'DigitalshiftCalendarBundle:Weekly:appointment.html.twig';
        $prefix          = 'weekly';

        return $this->renderSpecialAppointmentTemplate($appointment, $defaultTemplate, $prefix);
    }

    /**
     * @param \Twig_Environment $environment
     * @param AppointmentInterface $appointment
     * @return string
     */
    public function renderMonthlyAppointment(\Twig_Environment $environment, AppointmentInterface $appointment)
    {
        $this->environment = $environment;
        $this->appointment = $appointment;

        $defaultTemplate = 'DigitalshiftCalendarBundle:Monthly:appointment.html.twig';
        $prefix          = 'monthly';

        return $this->renderSpecialAppointmentTemplate($appointment, $defaultTemplate, $prefix);
    }

    /**
     * @param \Twig_Environment $environment
     * @param AppointmentInterface $appointment
     */
    public function renderListAppointment(\Twig_Environment $environment, AppointmentInterface $appointment)
    {
        $this->environment = $environment;
        $this->appointment = $appointment;

        $defaultTemplate = 'DigitalshiftCalendarBundle:List:appointment.html.twig';
        $prefix          = 'list';

        return $this->renderSpecialAppointmentTemplate($appointment, $defaultTemplate, $prefix);
    }

    /**
     * @param \Twig_Environment $environment
     * @param Holiday $holiday
     */
    public function renderListHoliday(\Twig_Environment $environment, Holiday $holiday)
    {
        $this->environment = $environment;

        $defaultTemplate = 'DigitalshiftCalendarBundle:List:holiday.html.twig';
        $prefix          = 'list';

        return $this->renderSpecialHolidayTemplate($holiday, $defaultTemplate, $prefix);
    }

    /**
     * @param Holiday $holiday
     * @param string $defaultTemplate
     * @param string $prefix
     * @return mixed
     */
    private function renderSpecialHolidayTemplate(Holiday $holiday, $defaultTemplate, $prefix)
    {
        $templateName = ($this->viewFolder) ?
            $this->viewFolder . ':'.$prefix.'Holiday.html.twig' :
            $defaultTemplate;

        $params = array('holiday' => $holiday);

        try {
            $content = $this->environment->render(
                $templateName,
                $params
            );
        }
        catch (\Twig_Error_Loader $e)
        {
            $content = $this->environment->render(
                $defaultTemplate,
                $params
            );
        }

        return $content;
    }

    /**
     * @param \Twig_Environment $environment
     * @param AppointmentInterface $appointment
     * @return string
     */
    public function appointmentInfo(\Twig_Environment $environment, AppointmentInterface $appointment)
    {
        $this->environment = $environment;
        $this->appointment = $appointment;

        $defaultTemplate = 'DigitalshiftCalendarBundle:Appointment:info.html.twig';
        $prefix          = 'info';

        return $this->renderSpecialAppointmentTemplate($appointment, $defaultTemplate, $prefix);
    }

    /**
     * @param AppointmentInterface $appointment
     * @param string $defaultTemplate
     * @param string $prefix
     */
    private function renderSpecialAppointmentTemplate(AppointmentInterface $appointment, $defaultTemplate, $prefix)
    {
        $appointmentExtension           = $this->appointmentExtensionService->getExtension($appointment);
        $appointmentExtensionClass      = $this->appointmentExtensionService->getExtensionClassName($appointmentExtension);
        $appointmentExtensionMappingKey = $this->appointmentExtensionService->getMappingKey($appointmentExtensionClass);

        $templateName = ($appointmentExtension) ?
            $appointmentExtension->getViewBaseFolder() . ':'.$prefix.ucfirst($appointmentExtensionMappingKey).'.html.twig' :
            $defaultTemplate;

        $params = array('appointment' => $appointment);

        try {
            $content = $this->environment->render(
                $templateName,
                $params
            );
        }
        catch (\Twig_Error_Loader $e)
        {
            $content = $this->environment->render(
                $defaultTemplate,
                $params
            );
        }

        return $content;
    }
}