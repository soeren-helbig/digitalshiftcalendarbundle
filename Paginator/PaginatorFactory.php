<?php

namespace Digitalshift\CalendarBundle\Paginator;

use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;

/**
 * PaginatorFactory
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class PaginatorFactory implements PaginatorFactoryInterface
{
    /** @var \Symfony\Bundle\FrameworkBundle\Routing\Router */
    private $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @{inheritdoc}
     */
    public function fromRequestFactory(Request $request, $route, $range = DateTimeHelper::TIME_WEEK)
    {
        $startDate = $this->getStartDate($request);

        $paginator = new Paginator();
        $paginator->setCurrent($this->getCurrentLabel($startDate, $range));
        $paginator->setPrevLinkTarget($this->getPageLinkPrev($startDate, $route));
        $paginator->setNextLinkTarget($this->getPageLinkNext($startDate, $route));
        $paginator->setResetLinkTarget($this->getPageLinkReset($route));

        return $paginator;
    }

    /**
     * @param Request $request
     * @return \DateTime
     */
    private function getStartDate(Request $request)
    {
        return new \DateTime($request->get('date', null));
    }

    /**
     * @param \DateTime $startDate
     * @param integer $range
     * @return string
     */
    private function getCurrentLabel(\DateTime $startDate, $range)
    {
        switch ($range) {
            case DateTimeHelper::TIME_WEEK:
                return sprintf('KW %d', $startDate->format('W'));
        }
    }

    /**
     * @param \DateTime $startDate
     * @param string $routeName
     * @return string
     */
    private function getPageLinkPrev(\DateTime $startDate, $routeName)
    {
        $date = clone $startDate;
        $date->sub(new \DateInterval('P1W'));

        return $this->router->generate(
            $routeName,
            array(
                'date' => $date->format('Y-m-d')
            )
        );
    }

    /**
     * @param \DateTime $startDate
     * @param string $routeName
     * @return string
     */
    private function getPageLinkNext(\DateTime $startDate, $routeName)
    {
        $date = clone $startDate;
        $date->add(new \DateInterval('P1W'));

        return $this->router->generate(
            $routeName,
            array(
                'date' => $date->format('Y-m-d')
            )
        );
    }

    /**
     * @param string $routeName
     * @return string
     */
    private function getPageLinkReset($routeName)
    {
        return $this->router->generate($routeName);
    }
}