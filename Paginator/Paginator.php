<?php

namespace Digitalshift\CalendarBundle\Paginator;

/**
 * Paginator
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class Paginator
{
    /**
     * @var string
     */
    private $current;

    /**
     * @var string
     */
    private $prevLinkTarget;

    /**
     * @var string
     */
    private $nextLinkTarget;

    /**
     * @var string
     */
    private $resetLinkTarget;

    /**
     * @param string $current
     * @param string $nextLinkTarget
     * @param string $prevLinkTarget
     */
    function __construct($current = null, $nextLinkTarget = null, $prevLinkTarget = null, $resetLinkTarget = null)
    {
        $this->current = $current;
        $this->nextLinkTarget = $nextLinkTarget;
        $this->prevLinkTarget = $prevLinkTarget;
        $this->resetLinkTarget = $resetLinkTarget;
    }

    /**
     * @param string $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;
    }

    /**
     * @return string
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param string $nextLinkTarget
     */
    public function setNextLinkTarget($nextLinkTarget)
    {
        $this->nextLinkTarget = $nextLinkTarget;
    }

    /**
     * @return string
     */
    public function getNextLinkTarget()
    {
        return $this->nextLinkTarget;
    }

    /**
     * @param string $prevLinkTarget
     */
    public function setPrevLinkTarget($prevLinkTarget)
    {
        $this->prevLinkTarget = $prevLinkTarget;
    }

    /**
     * @return string
     */
    public function getPrevLinkTarget()
    {
        return $this->prevLinkTarget;
    }

    /**
     * @param string $resetLinkTarget
     */
    public function setResetLinkTarget($resetLinkTarget)
    {
        $this->resetLinkTarget = $resetLinkTarget;
    }

    /**
     * @return string
     */
    public function getResetLinkTarget()
    {
        return $this->resetLinkTarget;
    }


} 