<?php

namespace Digitalshift\CalendarBundle\Paginator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * PaginatorInterface
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
interface PaginatorFactoryInterface
{
    /**
     * @param Request $request
     * @param string $route
     * @param integer $range
     * @return Paginator
     */
    public function fromRequestFactory(Request $request, $route, $range = DateTimeHelper::TIME_WEEK);
}