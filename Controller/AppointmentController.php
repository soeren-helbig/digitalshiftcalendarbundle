<?php

namespace Digitalshift\CalendarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * AppointmentController
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class AppointmentController extends Controller
{
    /**
     * @param Request $request
     * @param integer $id
     */
    public function ajaxInfoAction(Request $request, $id)
    {
        $appointment = $this->get('digitalshift_calendar.crud.appointments')->read($id);
        $viewData    = $this->render(
            'DigitalshiftCalendarBundle:Appointment:ajaxInfo.html.twig',
            array(
                'appointment' => $appointment
            )
        );

        $responseData = json_encode(array(
            'html' => $viewData->getContent()
        ));

        $response = new Response($responseData);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}