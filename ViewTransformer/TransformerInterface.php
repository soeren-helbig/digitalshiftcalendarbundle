<?php

namespace Digitalshift\CalendarBundle\ViewTransformer;

use DateTime;
use Digitalshift\CalendarBundle\AppointmentWrappers\DayCollection;
use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use Digitalshift\CalendarBundle\AppointmentWrappers\Week;

/**
 * TransformerInterface for all transformer-objects from DayCollection into view-friendly
 * representation
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
interface TransformerInterface
{
    public function __construct(DateTimeHelper $dateTimeHelper);
}