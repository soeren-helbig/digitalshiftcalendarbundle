<?php

namespace Digitalshift\CalendarBundle\ViewTransformer\Month;

use Digitalshift\CalendarBundle\AppointmentWrappers\Month;
use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use Digitalshift\CalendarBundle\ViewTransformer\TransformerInterface;

/**
 * WeeklyViewTransformer, to transform DayCollection into an array of
 * DateTimeRow - objects.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class MonthlyViewTransformer implements TransformerInterface
{
    private $dateTimeHelper;

    /**
     * @param DateTimeHelper $dateTimeHelper
     */
    public function __construct(DateTimeHelper $dateTimeHelper)
    {
        $this->dateTimeHelper = $dateTimeHelper;
    }

    /**
     * @param Month $month
     */
    public function getView(Month $month)
    {
        return $month->getWeeks();
    }
}