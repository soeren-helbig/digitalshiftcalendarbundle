<?php

namespace Digitalshift\CalendarBundle\ViewTransformer\Week;

use DateTime;
use Digitalshift\CalendarBundle\AppointmentWrappers\Week;
use Digitalshift\CalendarBundle\Library\DateTimeHelper;
use Digitalshift\CalendarBundle\ViewTransformer\TransformerInterface;
use Digitalshift\CalendarBundle\ViewTransformer\Week\DayTimeCell;
use Digitalshift\CalendarBundle\ViewTransformer\Week\DayTimeRow;

/**
 * WeeklyViewTransformer, to transform DayCollection into an array of
 * DateTimeRow - objects.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class WeeklyViewTransformer implements TransformerInterface
{
    const DEFAULT_DURATION = 1800;            // 1/2 hour

    private $dateTimeHelper, $week, $dayTimeRange, $start, $end, $viewRepresentation;

    /**
     * @param DateTimeHelper $dateTimeHelper
     */
    public function __construct(DateTimeHelper $dateTimeHelper)
    {
        $this->dateTimeHelper = $dateTimeHelper;
        $this->week           = null;
        $this->dayTimeRange   = self::DEFAULT_DURATION;
        $this->start          = 0;
        $this->end            = 60*60*24-1;
        $this->viewRepresentation = array();
    }

    /**
     * @param Week $week
     * @param DateTime $start
     * @param integer $duration
     */
    public function getView(Week $week, $dayTimeRange = self::DEFAULT_DURATION, DateTime $startTime = null, DateTime $endTime = null)
    {
        $this->setUp($week, $dayTimeRange, $startTime, $endTime);

        for ($time = $this->start; $time < $this->end; $time += $this->dayTimeRange)
        {
            $this->viewRepresentation[] = $this->dayTimeFactory($time);
        }

        return $this->viewRepresentation;
    }

    /**
     * @param Week $week
     * @param integer $dayTimeRange
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    private function setUp(Week $week, $dayTimeRange = self::DEFAULT_DURATION, DateTime $startTime = null, DateTime $endTime = null)
    {
        $this->week         = $week;
        $this->dayTimeRange = $dayTimeRange;

        $this->start = ($startTime) ?
            $this->dateTimeHelper->getTimestampInDay($startTime) :
            $this->start;

        $this->end = ($endTime) ?
            $this->dateTimeHelper->getTimestampInDay($endTime) :
            $this->end;

        $this->viewRepresentation = array();
    }

    /**
     * @param integer $dayTime
     */
    private function dayTimeFactory($dayTime)
    {
        $dayTimesForWeek           = array();
        $appointmentsPerDayForWeek = $this->week->getAppointmentsForDailyTimeRange($dayTime, $this->dayTimeRange, false);

        foreach ($appointmentsPerDayForWeek as $day => $appointmentsForDay)
        {
            if ($this->isCellLocked($day, $dayTime))
            {
                $dayTimeCell = new DayTimeCell(array(), $dayTime, $this->dayTimeRange);
                $dayTimeCell -> setLocked(true);
            }
            else
            {
                $dayTimeCell = new DayTimeCell($appointmentsForDay, $dayTime, $this->dayTimeRange);
            }

            $dayTimesForWeek[] = $dayTimeCell;
        }

        return new DayTimeRow($dayTime, $this->dayTimeRange, $dayTimesForWeek);
    }

    /**
     * @param integer $day
     * @param integer $dayTime
     * @return boolean
     */
    private function isCellLocked($day, $dayTime)
    {
        $depth = 0;
        $dayTimeLastUnlocked = $dayTime;

        do
        {
            $overlyingCell = $this->getOverlyingCell($day, $dayTimeLastUnlocked);
            $dayTimeLastUnlocked -= $this->dayTimeRange;
            $depth++;
        }
        while ($overlyingCell && $overlyingCell->isLocked());

        $test =  ($overlyingCell) ? $overlyingCell->getMaxDuration() : false;
        $test1 = $depth * $this->dayTimeRange;

        return ($overlyingCell && $overlyingCell->getMaxDuration() > $depth * $this->dayTimeRange) ?
            true :
            false;
    }

    /**
     * @todo code this stuff!
     *
     * @param integer $day
     * @param integer $dayTime
     * @param mixed
     */
    private function getOverlyingCell($day, $dayTime)
    {
        $viewRepresentationLength = count($this->viewRepresentation);

        if ($viewRepresentationLength < 1)
        {
            return false;
        }

        $overlyingDayTime    = $dayTime - $this->dayTimeRange;
        $overlyingDayTimeRow = null;

        foreach ($this->viewRepresentation as $dayTimeRow)
        {
            if ($dayTimeRow->getStart() !== $overlyingDayTime)
            {
                continue;
            }

            $overlyingDayTimeRow = $dayTimeRow;
            break;
        }

        if (!$overlyingDayTimeRow)
        {
            throw new \LogicException();
        }

        $overlyingIndex  = $viewRepresentationLength - 1;
        $dayTimeRowCells = $overlyingDayTimeRow->getDayTimeCells();

        return $dayTimeRowCells[$day];
    }
}