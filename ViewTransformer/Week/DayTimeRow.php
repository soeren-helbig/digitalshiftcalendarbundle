<?php

namespace Digitalshift\CalendarBundle\ViewTransformer\Week;

use Digitalshift\CalendarBundle\Library\DateTimeHelper;

/**
 * DayTimeRow encapsulates all DayTimeCells for one week and some meta information
 * like time range.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class DayTimeRow
{
    private $start, $duration, $dayTimeCells;

    public function __construct($start = 0, $duration = DateTimeHelper::TIME_HOUR, $dayTimeCells = array())
    {
        $this->start = $start;
        $this->duration = $duration;
        $this->dayTimeCells = $dayTimeCells;
    }

    public function getDayTimeCells()
    {
        return $this->dayTimeCells;
    }

    public function getStart()
    {
        return $this->start;
    }
}