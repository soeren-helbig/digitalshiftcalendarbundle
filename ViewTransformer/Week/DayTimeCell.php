<?php

namespace Digitalshift\CalendarBundle\ViewTransformer\Week;

use DateTime;
use Digitalshift\CalendarBundle\Appointments\AppointmentCollection;

/**
 * DayTimeCell Object to encapsulate appointment informations for rendering
 * a calendars weekly view.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class DayTimeCell
{
    private $locked, $start, $duration, $appointments;

    /**
     * @param array $appointments
     * @param integer $start
     * @param integer $duration
     */
    public function __construct($appointments, $start, $duration)
    {
        $this->locked       = false;
        $this->appointments = $appointments;
        $this->start        = $start;
        $this->duration     = $duration;
    }

    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * @todo code this stuff!
     *
     * @return boolean $relative
     *
     * @return integer
     */
    public function getMaxDuration($relative = false)
    {
        $maxDuration = $this->duration;

        foreach ($this->appointments as $appointment)
        {
            $maxDuration = ($appointment->getDuration() > $maxDuration) ?
                $appointment->getDuration() :
                $maxDuration;
        }

        return ($relative) ? $maxDuration / $this->duration : $maxDuration;
    }

    /**
     * @return boolean
     */
    public function isLocked()
    {
        return ($this->locked) ? true : false;;
    }

    /**
     * @param boolean $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return integer
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @return integer
     */
    public function getEnd()
    {
        return $this->start + $this->duration;
    }
}