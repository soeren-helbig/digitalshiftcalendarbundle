<?php

namespace Digitalshift\CalendarBundle\Library;

/**
 * DateTimeRange
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de>
 * @copyright Digitalshift (c) 2013
 */
class DateTimeRange
{
    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $end;

    public function __construct(\DateTime $start, \DateTime $end)
    {
        $this->start = $start;
        $this->end   = $end;
    }

    /**
     * @param \DateTime $end
     * @return \DateTime
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this->end;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $start
     * @return \DateTime
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this->start;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

} 