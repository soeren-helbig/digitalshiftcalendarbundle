<?php

namespace Digitalshift\CalendarBundle\Library;

use DateTime;
use InvalidArgumentException;
use Digitalshift\CalendarBundle\Library\DateTimeRange;

/**
 * DateService make some date calculation based on DateTime-Instances.
 *
 * @author Soeren Helbig <Soeren.Helbig@digitalshift.de
 * @copyright Digitalshift
 */
class DateTimeHelper
{
    const TIME_HOUR  = 3600;
    const TIME_DAY   = 86400;         // 1 day
    const TIME_WEEK  = 604800;        // 7 days
    const TIME_MONTH = 2592000;       // 30 days
    const TIME_YEAR  = 31536000;      // 365 days

    static $weekdays = array(
        1 => 'Montag',
        2 => 'Dienstag',
        3 => 'Mittwoch',
        4 => 'Donnerstag',
        5 => 'Freitag',
        6 => 'Samstag',
        7 => 'Sonntag'
    );
    
    public function __construct()
    {
    }

    /**
     * @param DateTime $date
     */
    public function getDayStart(DateTime $date)
    {
        $start =  new DateTime();
        $start -> setTimestamp(mktime(0, 0, 0, $date->format('n'), $date->format('j'), $date->format('Y')));

        return $start;
    }

    /**
     * @param DateTime $date
     */
    public function getDayEnd(DateTime $date)
    {
        $end =  new DateTime();
        $end -> setTimestamp(mktime(23, 59, 59, $date->format('n'), $date->format('j'), $date->format('Y')));

        return $end;
    }

    /**
     * @param DateTime $date
     */
    public function getDayRange(DateTime $date)
    {
        return new DateTimeRange($this->getDayStart($date), $this->getDayEnd($date));
    }

    /**
     * @param DateTime $date
     */
    public function getWeekStart(DateTime $date)
    {
        $weekNumber = $date->format('W');

        $start =  new DateTime();
        $start -> setTimestamp(strtotime($date->format('Y').'W'.$weekNumber));

        return $start;
    }

    /**
     * @param DateTime $date
     */
    public function getWeekEnd(DateTime $date)
    {
        $end = new DateTime();
        $end -> setTimestamp($this->getWeekStart($date)->getTimestamp() + self::TIME_WEEK -1);

        return $end;
    }

    /**
     * @param DateTime $date
     */
    public function getWeekRange(DateTime $date)
    {
        return new DateTimeRange($this->getWeekStart($date), $this->getWeekEnd($date));
    }

    /**
     * @param DateTime $date
     * @return array
     */
    public function getWeekRangePlain(DateTime $date)
    {
        $startTime = $date->getTimestamp();
        $endTime = $startTime + DateTimeHelper::TIME_WEEK;

        $end = new DateTime();
        $end->setTimestamp($endTime);

        return new DateTimeRange($date, $end);
    }

    /**
     * @param DateTime $start
     * @param DateTime $end
     *
     * @throws \InvalidArgumentException
     */
    public function getWeekRangesForMonth(DateTime $start, DateTime $end)
    {
        if ($end < $start)
        {
            throw new InvalidArgumentException();
        }

        $weeks = array();

        for($startTime = $start->getTimestamp(); $startTime <= $end->getTimestamp(); $startTime += self::TIME_WEEK)
        {
            $date = new DateTime();
            $date -> setTimestamp($startTime);

            $weeks [] = $this->getWeekRange($date);
        }

        return $weeks;
    }

    /**
     * @param DateTime $date
     * @return integer
     */
    public function getWeekNumber(DateTime $date)
    {
        return $date->format('W');
    }

    /**
     * @param DateTime $date
     * @param boolean $calendar - calendar month (with leading days in first week)
     * @return DateTime
     */
    public function getMonthStart(DateTime $date, $calendar = false)
    {
        $start = new DateTime();
        $start->setTimestamp(mktime(null,null,null,$date->format('m'),1,$date->format('Y')));

        if ($calendar)
        {
            $start = $this->getWeekStart($start);
        }

        return $start;
    }

    /**
     * @param DateTime $date
     * @param boolean $calendar - calendar month (with leading days in first week)
     * @return DateTime
     */
    public function getMonthEnd(DateTime $date, $calendar = false)
    {
        $end = new DateTime();
        $end->setTimestamp(mktime(null, null, null, $date->format('m'), $date->format('t'), $date->format('Y')));

        if ($calendar)
        {
            $end = $this->getWeekEnd($end);
        }

        return $end;
    }

    /**
     * @param DateTime $date
     * @return DateTime
     */
    public function getMonthRange(DateTime $date)
    {
        return new DateTimeRange(
            $this->getMonthStart($date, true),
            $this->getMonthEnd($date, true)
        );
    }

    /**
     * @param DateTime $date
     * @return array
     */
    public function getMonthRangePlain(DateTime $date)
    {
        return new DateTimeRange(
            $this->getMonthStart($date),
            $this->getMonthEnd($date)
        );
    }

    /**
     * @param DateTime $start
     * @param integer $range
     */
    public function getDateRange(DateTime $start, $range)
    {
        $start = $this->getWeekStart($start);

        $endTime = $start->getTimestamp() + $range;
        $end = new DateTime();
        $end->setTimestamp($endTime);

        $end = $this->getWeekEnd($end);

        return new DateTimeRange($start, $end);
    }

    /**
     * @param DateTime $start
     * @param integer $range
     */
    public function getDateRangePlain(DateTime $start, $range)
    {
        $endTime = $start->getTimestamp() + $range;
        $end = new DateTime();
        $end->setTimestamp($endTime);

        return new DateTimeRange($start, $end);
    }

    /**
     * @param DateTime $date
     *
     * @return integer
     */
    public function getTimestampInDay(DateTime $date)
    {
        return $date->format('G') * 60 * 60 + $date->format('i') * 60 + $date->format('s');
    }
    
    /**
     * @param integer $weekday
     * @return mixed
     */
    public function getWeekDayForIndex($weekday)
    {
        if (!isset(self::$weekdays[$weekday])) {
            throw new \InvalidArgumentException();
        }

        return self::$weekdays[$weekday];
    }
}